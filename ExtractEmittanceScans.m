function [ data ] = ExtractEmittanceScans( config, fill, additionalDataToQuery )
%EXTRACTEMITTANCESCANS Extract all scans for a fill from the logging

%#ok<*AGROW>

%% config
c = 2.998e8;

%% extract
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor;
dataMiner.setFill(fill);

try
    dataMiner.getFillStart()
    dataMiner.getFillEnd()
catch e
    fprintf('STABLE mode not found ... extracting from ADJUST for 10h.\n');
    dataMiner.setFill(fill,'ADJUST');
    endOfAdjust = dataMiner.getFillStart();
    dataMiner.setStartTimestamp(endOfAdjust);
    dataMiner.setEndTimestamp(endOfAdjust + 10*3600e3);
end

%% extract scans

ip = dataMiner.fetchDataSetTable('AUTOMATICSCAN:IP');
if isempty(ip)
    data = [];
    return
end
idx = find(ip(:,2) == 2^config.IP);
scanTimeWindows = [ ip(idx,1)-5e3 ip(idx+1,1)+5e3 ];
fprintf('Identified %d scans in IP %d, extracting ...\n', size(scanTimeWindows,1), config.IP);

dataMiner.setFill(fill, 'INJPHYS', 'RAMP');
filledBuckets.b1 = dataMiner.fetchDataSetTable('LHC.BQM.B1:FILLED_BUCKETS');
filledBuckets.b2 = dataMiner.fetchDataSetTable('LHC.BQM.B2:FILLED_BUCKETS');

for scan=1:size(scanTimeWindows)
    fprintf('extracting scan %d ...\n', scan);
    dataMiner.setStartTimestamp(scanTimeWindows(scan,1));
    dataMiner.setEndTimestamp(scanTimeWindows(scan,2));
    
    data{scan}.raw.acqFlag = dataMiner.fetchDataSetTable('AUTOMATICSCAN:ACQUISITION_FLAG', 'CMS:BUNCH_LUMI_INST');
    data{scan}.raw.nomDisp.xing.b1 = dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B1_XING', 'CMS:BUNCH_LUMI_INST');
    data{scan}.raw.nomDisp.xing.b2 = dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B2_XING', 'CMS:BUNCH_LUMI_INST');
    data{scan}.raw.nomDisp.sep.b1 = dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B1_SEP', 'CMS:BUNCH_LUMI_INST');
    data{scan}.raw.nomDisp.sep.b2 = dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B2_SEP', 'CMS:BUNCH_LUMI_INST');
    data{scan}.raw.totalLumi = dataMiner.fetchDataSetTable('CMS:LUMI_TOT_INST','CMS:BUNCH_LUMI_INST');
    data{scan}.raw.bunchLumi = dataMiner.fetchDataSetTable('CMS:BUNCH_LUMI_INST');
    data{scan}.raw.bunchLengths.b1 = dataMiner.fetchDataSetTable('LHC.BQM.B1:BUNCH_LENGTHS');
    data{scan}.raw.bunchLengths.b2 = dataMiner.fetchDataSetTable('LHC.BQM.B2:BUNCH_LENGTHS', 'LHC.BQM.B1:BUNCH_LENGTHS');
    
    if exist('additionalDataToQuery', 'var')
        for additionalIndex = 1:numel(additionalDataToQuery)
            fprintf('Querying additional item #%d ==> %s\n', additionalIndex, additionalDataToQuery{additionalIndex});
            data{scan}.additional{additionalIndex} = dataMiner.fetchDataSetTable(additionalDataToQuery{additionalIndex}); 
        end
    end

    data{scan}.meta.filledSlots.b1 = (filledBuckets.b1(end,find(filledBuckets.b1(end,2:end)>0)+1)-1)./10+1;
    if (isempty(data{scan}.meta.filledSlots.b1))
        data{scan}.meta.filledSlots.b1 = (filledBuckets.b1(end-1,find(filledBuckets.b1(end-1,2:end)>0)+1)-1)./10+1;
    end
    data{scan}.meta.filledSlots.b2 = (filledBuckets.b2(end,find(filledBuckets.b2(end,2:end)>0)+1)-1)./10+1;
    if (isempty(data{scan}.meta.filledSlots.b2))
        data{scan}.meta.filledSlots.b2 = (filledBuckets.b2(end-1,find(filledBuckets.b2(end-1,2:end)>0)+1)-1)./10+1;
    end
    
    colliding = CollidingBunchesIP(data{scan}.meta.filledSlots.b1, data{scan}.meta.filledSlots.b2, config.IP);
    data{scan}.meta.colliding.b1 = colliding(1,:);
    data{scan}.meta.colliding.b2 = colliding(2,:);
end



%% parse scans
for scan=1:size(scanTimeWindows)
    %% parse scan
    for plane='hv'
        relPlane = config.planes.(plane);
        separation = (data{scan}.raw.nomDisp.(relPlane).b1(:,2)-data{scan}.raw.nomDisp.(relPlane).b2(:,2));
        steps = unique(separation);
        steps = steps(~isnan(steps));
        
        if numel(steps) <= 1
            fprintf('Warning: skipping scan %d %s, no data\n', scan, plane);
            continue
        end
        % windowing: only consider points in the scan
        firstPoint = find(separation == min(steps),1,'first');
        lastPoint = find(separation == max(steps),1,'last');
        window = false(size(separation));
        window(firstPoint:lastPoint) = true;
        
        bunchLength.b1 = zeros(3564,1);
        bunchLength.b1(data{scan}.meta.filledSlots.b1,1) = nanmean(data{scan}.raw.bunchLengths.b1(:,(1:numel(data{scan}.meta.filledSlots.b1))+1),1);
        bunchLength.b2 = zeros(3564,1);
        bunchLength.b2(data{scan}.meta.filledSlots.b2,1) = nanmean(data{scan}.raw.bunchLengths.b2(:,(1:numel(data{scan}.meta.filledSlots.b2))+1),1);
        data{scan}.(plane).bunchLength = zeros(1,3564);
        data{scan}.(plane).bunchLength(1,data{scan}.meta.colliding.b1) =  (bunchLength.b1(data{scan}.meta.colliding.b1) + ...
            bunchLength.b2(data{scan}.meta.colliding.b2)) .* 0.5 .* c ./ 4;

        data{scan}.(plane).crossingAngle = config.crossingAngles.(plane);

        data{scan}.(plane).bump = steps;
        for step=1:numel(steps)
            idx = (separation==steps(step)) & (data{scan}.raw.acqFlag(:,2)==1) & window;
            data{scan}.(plane).totalLumi(step,1) = mean(data{scan}.raw.totalLumi(idx,2));
            data{scan}.(plane).bunchLumi(step,:) = mean(data{scan}.raw.bunchLumi(idx,2:end),1);
        end
    end
end
end

