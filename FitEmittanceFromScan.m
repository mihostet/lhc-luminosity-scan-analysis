function [ result, errors, fitresult ] = FitEmittanceFromScan( config, bump, lumi, bunchLen, crossingAngle )
%EMITTANCEFROMSCAN fit a gaussian and derive normalized emittances for a single bunch/scan

bunch_len_err = 0.01;

ft = fittype('gauss1');
opts = fitoptions(ft);
[fitresult, ~] = fit(bump, lumi, ft, opts );

params = confint(fitresult, 0.68); % 1 sigma = 68%
fitvalues = mean(params,1); fiterrors = diff(params,1);

%fprintf('  GOF - R^2:   %f\n', goodness.rsquare);
%fprintf('  Peak:   %f +- %f\n', fitvalues(1), fiterrors(1));
%fprintf('  Center: %f +- %f mm\n', fitvalues(2), fiterrors(2));
%fprintf('  Sigma:  %f +- %f mm\n', fitvalues(3)/sqrt(2), fiterrors(3)/sqrt(2));

result.peak = fitvalues(1);
errors.peak = fiterrors(1);
result.optimum = fitvalues(2);
errors.optimum = fiterrors(2);
result.kapsigma = fitvalues(3)* 1e-3/sqrt(2);
errors.kapsigma = fiterrors(3)* 1e-3/sqrt(2);

errors.fit_rmse = sqrt(sum((lumi-fitresult(bump)).^2));

% sqrt(2)^2 factors:
%   - 'gauss1' models does not contain the 2 factor in the exponential
%   - convolutions of two gaussians gives sigma^2 = sigma1^2+sigma2^2,
%       then assuming equal beams ...
sigma = fitvalues(3) * 1e-3/(sqrt(2)*sqrt(2));
sigma_error = fiterrors(3) * 1e-3/(sqrt(2)*sqrt(2));
if(crossingAngle > 0)
    sigma_error_factor = sigma / (sqrt(sigma^2 - bunchLen^2*sin(crossingAngle/2)^2) * cos(crossingAngle/2));
    bunchlen_error_factor = -bunchLen*sin(crossingAngle/2)^2 / (sqrt(sigma^2 - bunchLen^2*sin(crossingAngle/2)^2) * cos(crossingAngle/2));
    
%    fprintf('     correcting sigma for crossing: %f um ', sigma*1e6);
    sigma = sqrt(sigma^2 - bunchLen^2*sin(crossingAngle/2)^2) / cos(crossingAngle/2);
%    fprintf(' =>  %f um \n', sigma*1e6);
    sigma_error = sqrt( (sigma_error_factor*sigma_error)^2 + (bunchlen_error_factor*bunch_len_err)^2 );
end
result.emittance = 1e6 * (sigma)^2 * (config.energy/0.938) / config.betaStar;

errors.emittance = (sigma_error) * 1e6 * sigma * 2 * (config.energy/0.938) / config.betaStar;
%fprintf('==> Emittance:  %f +- %f \n', result.emittance, errors.emittance);


end

