function [ config ] = ConfigForIP5( )
%CONFIGFORIP5 returns a config struct for OP scans in IP5

config.IP = 5;
config.planes.h = 'xing';
config.planes.v = 'sep';
config.crossingAngles.h = 185e-6 * 2;
config.crossingAngles.v = 0;
config.energy = 6500;
config.betaStar = 0.4;


end

