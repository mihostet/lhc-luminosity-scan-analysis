function [ result, errors ] = BunchEmittancesFromScan( config, scan )
%BUNCHEMITTANCESFROMSCAN get the bunch-by-bunch emittances

for plane='hv'
    [result.(plane)(1:3564).peak] = deal(NaN);
    [result.(plane)(1:3564).optimum] = deal(NaN);
    [result.(plane)(1:3564).kapsigma] = deal(NaN);
    [result.(plane)(1:3564).emittance] = deal(NaN);

    [errors.(plane)(1:3564).peak] = deal(NaN);
    [errors.(plane)(1:3564).optimum] = deal(NaN);
    [errors.(plane)(1:3564).kapsigma] = deal(NaN);
    [errors.(plane)(1:3564).fit_rmse] = deal(NaN);
    [errors.(plane)(1:3564).emittance] = deal(NaN);
end
for bunch=scan.meta.colliding.b1
    try
        [ result.h(bunch), errors.h(bunch) ] = FitEmittanceFromScan(config, scan.h.bump, scan.h.bunchLumi(:,bunch), scan.h.bunchLength(bunch), scan.h.crossingAngle);
    catch e %#ok<*NASGU>
    end
    
    try
        [ result.v(bunch), errors.v(bunch) ] = FitEmittanceFromScan(config, scan.v.bump, scan.v.bunchLumi(:,bunch), scan.v.bunchLength(bunch), scan.v.crossingAngle);
    catch e
    end
end
end

