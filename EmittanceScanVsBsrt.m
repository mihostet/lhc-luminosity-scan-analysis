config = ConfigForIP5();
data = ExtractEmittanceScans(ConfigForIP5(), 5085);
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('MDB_PRO');
dataMiner.setFill(5085,'STABLE');
bsrt_B1_V = dataMiner.fetchDataSetTable('LHC.BSRT.5R4.B1:FIT_SIGMA_V');
bsrt_B2_V = dataMiner.fetchDataSetTable('LHC.BSRT.5L4.B2:FIT_SIGMA_V');
bsrt_B1_delay = dataMiner.fetchDataSetTable('LHC.BSRT.5R4.B1:GATE_DELAY');
bsrt_B2_delay = dataMiner.fetchDataSetTable('LHC.BSRT.5L4.B2:GATE_DELAY');
%%
b1V=bsrt_bbb(bsrt_B1_V, bsrt_B1_delay, 0.294, 330, 6500);
b2V=bsrt_bbb(bsrt_B2_V, bsrt_B2_delay, 0.299, 330, 6500);
%%
idx = 1;
[~,p1]=min(abs(min(data{idx}.raw.totalLumi(:,1))-b1V(:,1)))
[~,p2]=min(abs(min(data{idx}.raw.totalLumi(:,1))-b2V(:,1)))
[result,errors] = BunchEmittancesFromScan(config, data{idx});
%%
bsrt_emit_v = (mean(b1V(p1:p1+2000,2:end),1) + mean(b2V(p2:p2+2000,2:end),1))./2;
bsrt_emit_v(bsrt_emit_v==0) = NaN;
cms_emit=[result.v.emittance];

plot(bsrt_emit_v)
hold all
plot([result.v.emittance])
xlabel('bunch number [25ns slots]')
ylabel('emittance [um]')
legend('BSRT V', 'CMS mini-scan V')

