clear

fill = 5246
prof_B1_Bef=h5read('5246/PROFILE_B1_b1_scan1_20160825021711.h5','/Profile/deconvoluted');
prof_B2_Bef=h5read('5246/PROFILE_B2_b1_scan1_20160825021835.h5','/Profile/deconvoluted');

figure
hold all
for deltaRf=-0.0009:0.00001:0.0009
    
    scope.B1 = struct('samplerate', 40e9, 'fRF', 400.7900e6 + deltaRf*1e6);
    scope.B2 = struct('samplerate', 40e9, 'fRF', 400.7900e6 + deltaRf*1e6);
    
    [bunch_B1_Bef_timeref, bunch_B1_Bef, scope.B1] = SplitScopeAcqToBunches(scope.B1, prof_B1_Bef, 5246, 1);
    [bunch_B2_Bef_timeref, bunch_B2_Bef, scope.B2] = SplitScopeAcqToBunches(scope.B2, prof_B2_Bef, 5246, 2);
    [~,p1]=max(bunch_B1_Bef,[],2);
    [~,p2]=max(bunch_B2_Bef,[],2);
    spread1 = mean(abs(p1-51));
    spread2 = mean(abs(p2-51));

    fprintf('dRF = %.5f MHz ==> peak spread = %.2f, %.2f\n', deltaRf, spread1, spread2);

    plot(deltaRf, spread1, 'bo');
    plot(deltaRf, spread2, 'ro');
    
end