function [ wires_centered ] = CenterWires( wires )
%CENTERWIRES interpolates and centers multiple wire scans

%% interpolate wires
wires_processed.ref = -10000:50:10000; % um

for scan=1:size(wires.B1H.b_0,1)
    fprintf('B1H scan #%d taken at %s\n', scan, datestr(wires.B1H.b_0{scan,1}));
    wires_processed.B1H(scan,:) = interp1(wires.B1H.b_0{scan,2}(1,10:end-10),wires.B1H.b_0{scan,2}(2,10:end-10), wires_processed.ref);
end
for scan=1:size(wires.B1V.b_0,1)
    fprintf('B1V scan #%d taken at %s\n', scan, datestr(wires.B1V.b_0{scan,1}));
    wires_processed.B1V(scan,:) = interp1(wires.B1V.b_0{scan,2}(1,10:end-10),wires.B1V.b_0{scan,2}(2,10:end-10), wires_processed.ref);
end
for scan=1:size(wires.B2H.b_0,1)
    fprintf('B2H scan #%d taken at %s\n', scan, datestr(wires.B2H.b_0{scan,1}));
    wires_processed.B2H(scan,:) = interp1(wires.B2H.b_0{scan,2}(1,10:end-10),wires.B2H.b_0{scan,2}(2,10:end-10), wires_processed.ref);
end
for scan=1:size(wires.B2V.b_0,1)
    fprintf('B2V scan #%d taken at %s\n', scan, datestr(wires.B2V.b_0{scan,1}));
    wires_processed.B2V(scan,:) = interp1(wires.B2V.b_0{scan,2}(1,10:end-10),wires.B2V.b_0{scan,2}(2,10:end-10), wires_processed.ref);
end

%% center wires
wires_centered.ref = (-50*150):50:(50*150);
[~,p] = max(wires_processed.B1H(:,:)');
for scan=1:size(wires_processed.B1H,1)
	wires_centered.B1H(scan,:) = wires_processed.B1H(scan,(p(scan)-150):(p(scan)+150));
end
[~,p] = max(wires_processed.B1V(:,:)');
for scan=1:size(wires_processed.B1V,1)
	wires_centered.B1V(scan,:) = wires_processed.B1V(scan,(p(scan)-150):(p(scan)+150));
end
[~,p] = max(wires_processed.B2H(:,:)');
for scan=1:size(wires_processed.B2H,1)
	wires_centered.B2H(scan,:) = wires_processed.B2H(scan,(p(scan)-150):(p(scan)+150));
end
[~,p] = max(wires_processed.B2V(:,:)');
for scan=1:size(wires_processed.B2V,1)
	wires_centered.B2V(scan,:) = wires_processed.B2V(scan,(p(scan)-150):(p(scan)+150));
end

end

