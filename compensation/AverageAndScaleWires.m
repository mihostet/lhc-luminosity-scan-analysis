function [ wires_avg ] = AverageAndScaleWires( wires_centered, range, config )
%AVERAGEWIRES average and rescale wire scans to match the beta function at
% the IP

% WS beta from 2016 JMad model
%BWS.5R4.B1
%false	betx	194.6768054
%false	bety	368.2824385
%BWS.5L4.B2
%false	betx	188.910799
%false	bety	411.3553546range = 96:124;

wires_avg.B1H(:,1) = wires_centered.ref .* sqrt(config.betaStar/194.6768) .* 1e-6;
wires_avg.B1H(:,2) = mean(wires_centered.B1H(range,:),1);
wires_avg.B1H(:,2) = wires_avg.B1H(:,2) - mean(wires_avg.B1H([1:100 end-100:end],2),1);

wires_avg.B1V(:,1) = wires_centered.ref .* sqrt(config.betaStar/368.2824) .* 1e-6;
wires_avg.B1V(:,2) = mean(wires_centered.B1V(range,:),1);
wires_avg.B1V(:,2) = wires_avg.B1V(:,2) - mean(wires_avg.B1V([1:100 end-100:end],2),1);

wires_avg.B2H(:,1) = wires_centered.ref .* sqrt(config.betaStar/188.9108) .* 1e-6;
wires_avg.B2H(:,2) = mean(wires_centered.B2H(range,:),1);
wires_avg.B2H(:,2) = wires_avg.B2H(:,2) - mean(wires_avg.B2H([1:100 end-100:end],2),1);

wires_avg.B2V(:,1) = wires_centered.ref .* sqrt(config.betaStar/411.3554) .* 1e-6;
wires_avg.B2V(:,2) = mean(wires_centered.B2V(range,:),1);
wires_avg.B2V(:,2) = wires_avg.B2V(:,2) - mean(wires_avg.B2V([1:100 end-100:end],2),1);

end

