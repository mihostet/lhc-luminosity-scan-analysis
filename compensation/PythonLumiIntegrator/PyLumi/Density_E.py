import numpy as np
from numpy import sin,cos
class Density_E:
    _betaStarX = 0.0;
    _betaStarY = 0.0;
    _sinXingX = 0.0;
    _cosXingX = 0.0;
    _sinXingY = 0.0;
    _cosXingY = 0.0;
    _sepX = 0.0;
    _sepY = 0.0;
    _emitX = 0.0;
    _emitY = 0.0;
    _sigS = 0.0;
    _sepS = 0.0;
    _crabCavityKX = 0.0;
    _crabCavityKY = 0.0;
    _sincrabAngleX = 0.0;
    _sincrabAngleY = 0.0;
    beam = 1;
    
    #Betastar in [m]
    #half Xing in [rad]
    #half sep in beam sigma
    #physical emittance in [m rad]
    #sigs in m
    #seps in m (longitudinal cogging)
    #crab cavity angle (sign convention : for full crabbing use crabAngl = half Xing angle)
    #crab cavity wave number k=2pi*frequecy/c
    def __init__(self,betaStarX=0.55,betaStarY=0.55,xingX=0.0,xingY=0.0,sepX=0.0,sepY=0.0,emitX=0.0,emitY=0.0,sigS=0.0,sepS=0.0,crabCavityKX=0.0,crabCavityKY=0.0,crabAngleX=0.0,crabAngleY=0.0,beam=1):
        self._betaStarX=betaStarX;
        self._betaStarY=betaStarY;
        self._sinXingX=np.sin(xingX);
        self._cosXingX=np.cos(xingX);
        self._sinXingY=np.sin(xingY);
        self._cosXingY=np.cos(xingY);
        self._sincrabAngleX=np.sin(crabAngleX);
        self._sincrabAngleY=np.sin(crabAngleY);
        self._sepX=sepX;
        self._sepY=sepY;
        self._emitX=emitX;
        self._emitY=emitY;
        self._sigS=sigS;
        self._sepS=sepS;
        self._crabCavityKX=crabCavityKX
        self._crabCavityKY=crabCavityKY
	self.beam = beam
    
    def getX(self,x,y,s,s0):
        if abs(self._crabCavityKX) < 0.0001:
            return x*self._cosXingX-s*self._sinXingX + self._sepX + (s-s0)*self._sincrabAngleX
        else:
            return x*self._cosXingX-s*self._sinXingX + self._sepX + np.sin(self._crabCavityKX*(s-s0))*self._sincrabAngleX/self._crabCavityKX;
    def getY(self,x,y,s,s0):
        if abs(self._crabCavityKY) < 0.0001:
            return y*self._cosXingY-s*self._sinXingY + self._sepY + (s-s0)*self._sincrabAngleY;
        else:
            return y*self._cosXingY-s*self._sinXingY + self._sepY + np.sin(self._crabCavityKY*(s-s0))*self._sincrabAngleY/self._crabCavityKY;
    def getS(self,x,y,s,s0):
        return (s-s0+self._sepS)*self._cosXingX*self._cosXingY;
    
    def sigX(self,x,y,s,s0):
        return np.sqrt(self._betaStarX*(1.0+(s/self._betaStarX)**2)*self._emitX);
        #return np.sqrt(self._betaStarX*self._emitX);
    def sigY(self,x,y,s,s0):
        return np.sqrt(self._betaStarY*(1.0+(s/self._betaStarY)**2)*self._emitY);
        #return np.sqrt(self._betaStarY*self._emitY);

		
    def longDist(self,z):
     if self.beam==1:
       a0 =         101  
       a1 =       166.4 
       b1 =       2.047
       a2 =       89.07
       b2 =      0.5682 
       a3 =       24.14 
       b3 =      -1.597 
       a4 =      -10.14 
       b4 =      0.1921 
       a5 =      -16.85
       b5 =    -0.09885
       a6 =      -9.775  
       b6 =      -1.701
       w =       2.379
       scale = (1/79.8890090289)       
     elif self.beam == 2:
       a0 =       115.7 
       a1 =       182.1 
       b1 =      -9.734 
       a2 =       87.67
       b2 =      -8.595
       a3 =       14.22 
       b3 =      -3.859 
       a4 =      -16.17 
       b4 =       4.204
       a5 =       -18.3
       b5 =       2.942
       a6 =       -6.65
       b6 =    -0.04564
       w =       2.644
       scale = 1/81.9593531701
     x = (z/2.99e8)*1e9;
     LongDist=scale*(a0 + a1*cos(x*w) + b1*sin(x*w) + \
             a2*cos(2*x*w) + b2*sin(2*x*w) + a3*cos(3*x*w) + b3*sin(3*x*w) + \
             a4*cos(4*x*w) + b4*sin(4*x*w) + a5*cos(5*x*w) + b5*sin(5*x*w) + \
             a6*cos(6*x*w) + b6*sin(6*x*w));
     indxBAD=abs(x)>0.9;
     LongDist[indxBAD==1]=0;
     return 	LongDist;
		
    def __call__(self,x,y,s,s0):
        sigX = self.sigX(x,y,s,s0);
        sigY = self.sigY(x,y,s,s0);
        #return np.exp(-self.getX(x,y,s,s0)**2/(2.0*sigX**2)-self.getY(x,y,s,s0)**2/(2.0*sigY**2)-self.getS(x,y,s,s0)**2/(2.0*self._sigS**2))/(sigX*sigY*self._sigS*(2.0*np.pi)**(3.0/2.0));
        X_ax=self.getX(x,y,s,s0);
        Y_ax=self.getY(x,y,s,s0);
        S_ax=self.getS(x,y,s,s0);
#	print S_ax;
        S_1D=self.longDist(S_ax);
#	print ','.join([str(v) for v in S_1D])
        aaa=S_1D*np.exp(-X_ax**2/(2.0*sigX**2)-Y_ax**2/(2.0*sigY**2))/(sigX*sigY*(2.0*np.pi));
        return aaa;
