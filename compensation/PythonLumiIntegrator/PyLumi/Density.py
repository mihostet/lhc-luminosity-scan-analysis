import numpy as np

class Density:
    _betaStarX = 0.0;
    _betaStarY = 0.0;
    _sinXingX = 0.0;
    _cosXingX = 0.0;
    _sinXingY = 0.0;
    _cosXingY = 0.0;
    _sepX = 0.0;
    _sepY = 0.0;
    _emitX = 0.0;
    _emitY = 0.0;
    _sigS = 0.0;
    _sepS = 0.0;
    _crabCavityKX = 0.0;
    _crabCavityKY = 0.0;
    _sincrabAngleX = 0.0;
    _sincrabAngleY = 0.0;
    beam = 1;
    
    #Betastar in [m]
    #half Xing in [rad]
    #half sep in beam sigma
    #physical emittance in [m rad]
    #sigs in m
    #seps in m (longitudinal cogging)
    #crab cavity angle (sign convention : for full crabbing use crabAngl = half Xing angle)
    #crab cavity wave number k=2pi*frequecy/c
    def __init__(self,betaStarX=0.55,betaStarY=0.55,xingX=0.0,xingY=0.0,sepX=0.0,sepY=0.0,emitX=0.0,emitY=0.0,sigS=0.0,sepS=0.0,crabCavityKX=0.0,crabCavityKY=0.0,crabAngleX=0.0,crabAngleY=0.0,beam=1):
        self._betaStarX=betaStarX;
        self._betaStarY=betaStarY;
        self._sinXingX=np.sin(xingX);
        self._cosXingX=np.cos(xingX);
        self._sinXingY=np.sin(xingY);
        self._cosXingY=np.cos(xingY);
        self._sincrabAngleX=np.sin(crabAngleX);
        self._sincrabAngleY=np.sin(crabAngleY);
        self._sepX=sepX;
        self._sepY=sepY;
        self._emitX=emitX;
        self._emitY=emitY;
        self._sigS=sigS;
        self._sepS=sepS;
        self._crabCavityKX=crabCavityKX
        self._crabCavityKY=crabCavityKY
	self.beam = beam
    
    def getX(self,x,y,s,s0):
        if abs(self._crabCavityKX) < 0.0001:
            return x*self._cosXingX-s*self._sinXingX + self._sepX + (s-s0)*self._sincrabAngleX
        else:
            return x*self._cosXingX-s*self._sinXingX + self._sepX + np.sin(self._crabCavityKX*(s-s0))*self._sincrabAngleX/self._crabCavityKX;
    def getY(self,x,y,s,s0):
        if abs(self._crabCavityKY) < 0.0001:
            return y*self._cosXingY-s*self._sinXingY + self._sepY + (s-s0)*self._sincrabAngleY;
        else:
            return y*self._cosXingY-s*self._sinXingY + self._sepY + np.sin(self._crabCavityKY*(s-s0))*self._sincrabAngleY/self._crabCavityKY;
    def getS(self,x,y,s,s0):
        return (s-s0+self._sepS)*self._cosXingX*self._cosXingY;
    
    def sigX(self,x,y,s,s0):
        return np.sqrt(self._betaStarX*(1.0+(s/self._betaStarX)**2)*self._emitX);
        #return np.sqrt(self._betaStarX*self._emitX);
    def sigY(self,x,y,s,s0):
        return np.sqrt(self._betaStarY*(1.0+(s/self._betaStarY)**2)*self._emitY);
        #return np.sqrt(self._betaStarY*self._emitY);
		
    def longDist(self,z):
	return np.exp(-z**2/(2.0*self._sigS**2))/(self._sigS*(2.0*np.pi)**(1.0/2.0));
	
    def __call__(self,x,y,s,s0):
        sigX = self.sigX(x,y,s,s0);
        sigY = self.sigY(x,y,s,s0);
        #return np.exp(-self.getX(x,y,s,s0)**2/(2.0*sigX**2)-self.getY(x,y,s,s0)**2/(2.0*sigY**2)-self.getS(x,y,s,s0)**2/(2.0*self._sigS**2))/(sigX*sigY*self._sigS*(2.0*np.pi)**(3.0/2.0));
        X_ax=self.getX(x,y,s,s0);
        Y_ax=self.getY(x,y,s,s0);
        S_ax=self.getS(x,y,s,s0);
        S_1D=self.longDist(S_ax);#np.exp(-S_ax**2/(2.0*self._sigS**2))/(self._sigS*(2.0*np.pi)**(1.0/2.0));
	print len(S_1D)
	#print ','.join([str(v) for v in S_1D])
        #S_1D=self.longDistIniFill(S_ax);
        aaa=S_1D*np.exp(-X_ax**2/(2.0*sigX**2)-Y_ax**2/(2.0*sigY**2))/(sigX*sigY*(2.0*np.pi));
        return aaa;
		
class Overlap(Density):
    _densityB1 = None;
    _densityB2 = None;
    
    def __init__(self,densityB1,densityB2):
        self._densityB1 = densityB1;
        self._densityB2 = densityB2;
        
    def __call__(self,x,y,s,s0):
        return self._densityB1(x,y,s,s0)*self._densityB2(x,y,s,-s0);

