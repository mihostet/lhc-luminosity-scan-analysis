import numpy as np

#func has to be of the form func(var1,var2,arg1,arg2,density)
#var1 and var2 will vary in between boundary1 and boundary2 respectively
def integrale2D(func,density,boundary1,boundary2,arg1,arg2,order=10):
    stepSize1 = (boundary1[1]-boundary1[0])/order;
    stepSize2 = (boundary2[1]-boundary2[0])/order;
    steps1 = np.arange(boundary1[0]+stepSize1/2,boundary1[1],stepSize1);
    steps2 = np.arange(boundary2[0]+stepSize2/2,boundary2[1],stepSize2);
    ones = np.ones(np.shape(steps1));
    steps1 = np.kron(steps1,ones);
    steps2 = np.kron(ones,steps2);
    return np.sum(func(steps1,steps2,arg1,arg2,density))*stepSize1*stepSize2;

#func has to be of the form func(var1,var2,var3,arg,density)
#var1,var2 and var3 will vary in between boundary1, boundary2 and boundary3 respectively
def integrale3D(func,density,boundary1,boundary2,boundary3,arg,order=10):
    stepSize1 = (boundary1[1]-boundary1[0])/order;
    stepSize2 = (boundary2[1]-boundary2[0])/order;
    stepSize3 = (boundary3[1]-boundary3[0])/order;
    steps1 = np.arange(boundary1[0]+stepSize1/2,boundary1[1],stepSize1);
    steps2 = np.arange(boundary2[0]+stepSize2/2,boundary2[1],stepSize2);
    steps3 = np.arange(boundary3[0]+stepSize3/2,boundary3[1],stepSize3);
    ones = np.ones_like(steps1);
    steps1 = np.kron(steps1,ones);
    steps2 = np.kron(ones,steps2);
    steps3 = np.kron(ones,steps3);
    steps1 = np.kron(steps1,ones);
    steps2 = np.kron(steps2,ones);
    steps3 = np.kron(ones,steps3);
    return np.sum(func(steps1,steps2,steps3,arg,density))*stepSize1*stepSize2*stepSize3;

#func has to be of the form func(var1,var2,var3,var4,density)
#var1,var2,var3 and var4 will vary in between boundary1, boundary2, boundary3 and boundary4 respectively  
def integrale4D(func,density,boundary1,boundary2,boundary3,boundary4,order=10):
    stepSize1 = (boundary1[1]-boundary1[0])/order;
    stepSize2 = (boundary2[1]-boundary2[0])/order;
    stepSize3 = (boundary3[1]-boundary3[0])/order;
    stepSize4 = (boundary4[1]-boundary4[0])/order;
    steps1 = np.arange(boundary1[0]+stepSize1/2,boundary1[1],stepSize1);
    steps2 = np.arange(boundary2[0]+stepSize2/2,boundary2[1],stepSize2);
    steps3 = np.arange(boundary3[0]+stepSize3/2,boundary3[1],stepSize3);
    steps4 = np.arange(boundary4[0]+stepSize4/2,boundary4[1],stepSize4);
    ones = np.ones_like(steps1);
    steps1 = np.kron(steps1,ones);
    steps2 = np.kron(ones,steps2);
    steps3 = np.kron(ones,steps3);
    steps4 = np.kron(ones,steps4);
    steps1 = np.kron(steps1,ones);
    steps2 = np.kron(steps2,ones);
    steps3 = np.kron(ones,steps3);
    steps4 = np.kron(ones,steps4);
    steps1 = np.kron(steps1,ones);
    steps2 = np.kron(steps2,ones);
    steps3 = np.kron(steps3,ones);
    steps4 = np.kron(ones,steps4);
    return np.sum(func(steps1,steps2,steps3,steps4,density))*stepSize1*stepSize2*stepSize3*stepSize4;
