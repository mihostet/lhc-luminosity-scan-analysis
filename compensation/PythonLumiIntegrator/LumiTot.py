import numpy as np
import time

from PyLumi.Density import Density, Overlap
from PyLumi.Density_R import Density_R
from PyLumi.Density_E import Density_E
from PyLumi.Integrator import *
import matplotlib.pyplot as plt


def getOverlapXYSS0(x,y,s,s0,overlap):
    return overlap(x,y,s,s0);

if __name__ == '__main__':

    cLight = 299792458.0;

    sigs = 0.089#0.077;
    energy = 6.5E3;
    mass = 0.9382720;
    relGamma = 1+energy/mass;
    emitH = 2.7E-6/relGamma;
    emitV = 3.0E-6/relGamma;
    betaStar = 0.4;
    transverseBeamSizeH = np.sqrt(betaStar*emitH);
    transverseBeamSizeV = np.sqrt(betaStar*emitV);	
    nSigH = 10.0;
    nSigV = 10.0;
    nSigL = 10.0;	
    Xing = 2.0*185E-6;
    intensity_R = 1.03E11;
    intensity_E = 1.00E11;
    frev = 11245.0
    nBunch = 1;
    
    #seps = 85E-12*cLight
    #print seps/sigs
	
    numSteps=40;
    half_separation=np.linspace(-40e-6,40e-6,num=numSteps);
    allLumi = np.array([]);
    allLumi_R = np.array([]);
    allLumi_E = np.array([]);
    for i in range(0,numSteps):	
        densityB1_R = Density_R(beam=1,betaStarX=betaStar,betaStarY=betaStar,xingX=Xing/2,xingY=0.0,sepX=half_separation[i],sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
        densityB2_R = Density_R(beam=2,betaStarX=betaStar,betaStarY=betaStar,xingX=-Xing/2,xingY=0.0,sepX=-half_separation[i],sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
        densityB1 = Density(beam=1,betaStarX=betaStar,betaStarY=betaStar,xingX=Xing/2,xingY=0.0,sepX=half_separation[i],sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
        densityB2 = Density(beam=2,betaStarX=betaStar,betaStarY=betaStar,xingX=-Xing/2,xingY=0.0,sepX=-half_separation[i],sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
        densityB1_E = Density_E(beam=1,betaStarX=betaStar,betaStarY=betaStar,xingX=Xing/2,xingY=0.0,sepX=half_separation[i],sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
        densityB2_E = Density_E(beam=2,betaStarX=betaStar,betaStarY=betaStar,xingX=-Xing/2,xingY=0.0,sepX=-half_separation[i],sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
        overlap = Overlap(densityB1,densityB2);
        overlap_R = Overlap(densityB1_R,densityB2_R);
        overlap_E = Overlap(densityB1_E,densityB2_E);
    
    
	
	
        xBoundary = [-nSigH*transverseBeamSizeH, nSigH*transverseBeamSizeH];
        yBoundary = [-nSigV*transverseBeamSizeV, nSigV*transverseBeamSizeV];
        sBoundary = [-nSigL*sigs,nSigL*sigs];
        s0Boundary = [-nSigL*sigs,nSigL*sigs];
    
        time0 = time.time();
        bunchSpecLumi = 2E-4*np.cos(Xing)**2*integrale4D(getOverlapXYSS0,overlap,xBoundary,yBoundary,sBoundary,s0Boundary,order=40);
        specLumi = bunchSpecLumi*frev*nBunch
        bunchSpecLumi_R = 2E-4*np.cos(Xing)**2*integrale4D(getOverlapXYSS0,overlap_R,xBoundary,yBoundary,sBoundary,s0Boundary,order=40);
        specLumi_R = bunchSpecLumi_R*frev*nBunch;
        bunchSpecLumi_E = 2E-4*np.cos(Xing)**2*integrale4D(getOverlapXYSS0,overlap_E,xBoundary,yBoundary,sBoundary,s0Boundary,order=40);
        specLumi_E = bunchSpecLumi_E*frev*nBunch;
        
        lumiTot = specLumi*intensity_R**2;
        lumiTot_R = specLumi_R*intensity_R**2;
        lumiTot_E = specLumi_E*intensity_E**2;
        time1 = time.time();
        print('step %d' % i)
        print('Time for integration ',time1-time0,'s');
        print(lumiTot,lumiTot_R,lumiTot_E);
        #print(lumiTot,specLumi,visibleXSection*lumiTot/(frev*nBunch));
        allLumi=np.append(allLumi,lumiTot);
        allLumi_R=np.append(allLumi_R,lumiTot_R);
        allLumi_E=np.append(allLumi_E,lumiTot_E);
    plt.plot(half_separation*2*1e6, allLumi)
    plt.plot(half_separation*2*1e6, allLumi_R)
    plt.plot(half_separation*2*1e6, allLumi_E)
    plt.show()
    print('sim.scan.gauss=['+','.join([str(a) for a in allLumi])+']');
    print('sim.scan.init=['+','.join([str(a) for a in allLumi_R])+']');
    print('sim.scan.end=['+','.join([str(a) for a in allLumi_E])+']');
	
