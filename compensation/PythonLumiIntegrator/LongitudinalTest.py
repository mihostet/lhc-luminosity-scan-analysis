import numpy as np
import time

from PyLumi.Density import Density, Overlap
from PyLumi.Density_R import Density_R
from PyLumi.Density_E import Density_E
from PyLumi.Integrator import *
import matplotlib.pyplot as plt



cLight = 299792458.0;

sigs = 0.077;
energy = 6.5E3;
mass = 0.9382720;
relGamma = 1+energy/mass;
emitH = 3.3E-6/relGamma;
emitV = 3.3E-6/relGamma;
betaStar = 0.4;
transverseBeamSizeH = np.sqrt(betaStar*emitH);
transverseBeamSizeV = np.sqrt(betaStar*emitV);	
nSigH = 10.0;
nSigV = 10.0;
nSigL = 10.0;	
Xing = 2.0*185E-6;
intensity = 1.15E11;
frev = 11245.0
nBunch = 1;

for beam in [1,2]:
	print "beam %d" % beam
	densityB1_R = Density_R(beam=beam,betaStarX=betaStar,betaStarY=betaStar,xingX=Xing/2,xingY=0.0,sepX=0.0,sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
	densityB1_E = Density_E(beam=beam,betaStarX=betaStar,betaStarY=betaStar,xingX=Xing/2,xingY=0.0,sepX=0.0,sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
	densityB1 = Density(beam=beam,betaStarX=betaStar,betaStarY=betaStar,xingX=Xing/2,xingY=0.0,sepX=0.0,sepY=0.0,sepS=0.0,emitX=emitH,emitY=emitV,sigS=sigs);
	s=np.arange(-0.3,0.3,0.001);
	dR=densityB1_R.longDist(s);
	dE=densityB1_E.longDist(s);
	dG=densityB1.longDist(s);

	plt.plot(s,dR)
	plt.plot(s,dE)
	plt.plot(s,dG)

	s=np.arange(-1,1,0.001);
	print np.trapz(densityB1_R.longDist(s), s)
	print np.trapz(densityB1_E.longDist(s), s)
	print np.trapz(densityB1.longDist(s), s)


plt.show()