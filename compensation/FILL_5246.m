clear
get = @(i,s) s(i);

scans = load('5246/scans5246.mat');
wires = load('5246/Fill5246_WSProfiles.mat');

meta = scans.data{1}.meta;

[scans.resultH,scans.errorsH] = BunchEmittancesFromScan(scans.config, scans.data{2});
[scans.resultV,scans.errorsV] = BunchEmittancesFromScan(scans.config, scans.data{1});
%% intensities
load('5246/fbct5246.mat');
%{
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
dataMiner.setStartTimestamp(scans.data{1}.raw.totalLumi(1,1))
dataMiner.setEndTimestamp(scans.data{2}.raw.totalLumi(end,1))
intensities.B1 = dataMiner.fetchDataSetTable('LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
intensities.B2 = dataMiner.fetchDataSetTable('LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY');
%}

%% long profiles
load('5246/profiles5246.mat');

%profiles.B1.raw = h5read('5246/PROFILE_B1_b1_scan1_20160825021711.h5','/Profile/deconvoluted');
%profiles.B2.raw = h5read('5246/PROFILE_B2_b1_scan1_20160825021835.h5','/Profile/deconvoluted');
%{
profiles.B1.raw = mean(h5read('5246/PROFILE_B1_b1_scan1_20160825021711.h5','/Profile/profile'),1);
profiles.B2.raw = mean(h5read('5246/PROFILE_B2_b1_scan1_20160825021835.h5','/Profile/profile'),1);

%% split bunches
profiles.scope.B1 = struct('samplerate', 40e9, 'fRF', 400.79e6+0.00069e6, 'sampleLag', -35305);
profiles.scope.B2 = struct('samplerate', 40e9, 'fRF', 400.79e6-0.00010e6, 'sampleLag', -35499);

[profiles.B1.timeref, profiles.B1.bunch, profiles.scope.B1] = SplitScopeAcqToBunches(profiles.scope.B1, profiles.B1.raw, 5246, 1); 
[profiles.B2.timeref, profiles.B2.bunch, profiles.scope.B2] = SplitScopeAcqToBunches(profiles.scope.B2, profiles.B2.raw, 5246, 2); 

profiles.B1.bunch(:,51:end-1) = fliplr(profiles.B1.bunch(:,1:49));
profiles.B2.bunch(:,51:end-1) = fliplr(profiles.B2.bunch(:,1:49));
%}
%% center wires
wires_centered = CenterWires(wires);
wirescans_range = 96:124;
wires_avg = AverageAndScaleWires(wires_centered, wirescans_range, ConfigForIP5());

%% sim
bunchIdx = 1;
slot = 1;
%    emittances = struct('h',get(dataBefore.meta.colliding.b1(b),[resultBef.h.emittance]),'v',get(dataBefore.meta.colliding.b1(b),[resultBef.v.emittance]));
emittances = struct('h',3.5e-6,'v',4e-6);
averageIntensity = 0.5 .* (intensities.B1(1,slot+1) + intensities.B2(1,slot+1));
simulation_H = LumiForBunchProfile(ConfigForIP5(), profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'h');
simulation_V = LumiForBunchProfile(ConfigForIP5(),profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'v');
fprintf('Simulation done.\n');

%% sim with wire profiles
simulation_H_wireprofile = LumiForBunchProfile(ConfigForIP5(),profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'h', wires_avg );
simulation_V_wireprofile = LumiForBunchProfile(ConfigForIP5(),profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'v', wires_avg );
fprintf('Simulation done.\n');

%% sim with different xing angles
config = ConfigForIP5();
config.crossingAngles.h = 350e-6;
emittances = struct('h',3.5e-6,'v',4e-6);
simulation_H_lowxing = LumiForBunchProfile(config, profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'h');
simulation_V_lowxing = LumiForBunchProfile(config, profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'v');
fprintf('Simulation done.\n');

%% sim with different beta star
config = ConfigForIP5();
config.betaStar = 0.375;
emittances = struct('h',3.5e-6,'v',4e-6);
simulation_H_lowbeta = LumiForBunchProfile(config, profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'h');
simulation_V_lowbeta = LumiForBunchProfile(config, profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'v');
fprintf('Simulation done.\n');

%% sim with different beta star and WS profiles
config = ConfigForIP5();

config.crossingAngles.h = 340e-6;

simulation_H_lowbeta_wires = LumiForBunchProfile(config, profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'h', AverageAndScaleWires(wires_centered, wirescans_range, config));
simulation_V_lowbeta_wires = LumiForBunchProfile(config, profiles.B1.timeref, profiles.B1.bunch(bunchIdx,:), profiles.B2.timeref, profiles.B2.bunch(bunchIdx,:), ...
    emittances, averageIntensity, 'v', AverageAndScaleWires(wires_centered, wirescans_range, config));
fprintf('Simulation done.\n');


%% plot

scale = 0.92;
xoff = 0.0007;
yoff = 0.0009;

figure;
subplot(2,1,1);
hold all
plot(simulation_H.bump.*1e3, simulation_H.scan./1e30)
plot(simulation_H_wireprofile.bump.*1e3, simulation_H_wireprofile.scan./1e30)
plot(scans.data{2}.h.bump-xoff, scans.data{2}.h.bunchLumi(:,1),'o')
plot(scans.data{2}.h.bump-xoff, scans.data{2}.h.bunchLumi(:,1).*scale,'o')
%plot(simulation_H_lowbeta.bump.*1e3, simulation_H_lowbeta.scan./1e30)
%plot(simulation_H_lowxing.bump.*1e3, simulation_H_lowxing.scan./1e30)
plot(simulation_H_lowbeta_wires.bump.*1e3, simulation_H_lowbeta_wires.scan./1e30)

legend('simulation (gaussian, 3.5/4um)', 'simulation (WS profile)', 'CMS scan', 'CMS scan (92%)', 'sim (-5% angle, -7% beta*)');
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')
ylim([0 2.2])

subplot(2,1,2);
hold all
plot(simulation_V.bump.*1e3, simulation_V.scan./1e30)
plot(simulation_V_wireprofile.bump.*1e3, simulation_V_wireprofile.scan./1e30)
plot(scans.data{1}.v.bump-yoff, scans.data{1}.v.bunchLumi(:,1),'o')
plot(scans.data{1}.v.bump-yoff, scans.data{1}.v.bunchLumi(:,1).*scale,'o')
%plot(simulation_V_lowbeta.bump.*1e3, simulation_V_lowbeta.scan./1e30)
%plot(simulation_V_lowxing.bump.*1e3, simulation_V_lowxing.scan./1e30)
plot(simulation_V_lowbeta_wires.bump.*1e3, simulation_V_lowbeta_wires.scan./1e30)

legend('simulation (gaussian, 3.5/4um)', 'simulation (WS profile)', 'CMS scan', 'CMS scan (92%)', 'sim (-5% angle, -7% beta*)');
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')
ylim([0 2.2])


%% try to get emittances
cLight = 299792458;
config = ConfigForIP5();

distS.B1 = DistributionFromProfileData( profiles.B1.timeref.*cLight, profiles.B1.bunch(1,:), 'normalize');
distS.B2 = DistributionFromProfileData(-profiles.B2.timeref.*cLight, profiles.B2.bunch(1,:), 'normalize');
theLumi = scans.data{2}.h.bunchLumi(:,1);
theBump = scans.data{2}.h.bump.*1e-3;

analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.h, max(theLumi));
fitresult=fit(theBump, theLumi, fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
    'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
compensatedSigma = coeffvalues(fitresult);
compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar
compensatedOptimum = compensatedSigma(2) .* 1e6
%%
theLumi = scans.data{1}.v.bunchLumi(:,1);
theBump = scans.data{1}.v.bump.*1e-3;
analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.v, max(theLumi));
fitresult=fit(theBump, theLumi, fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
    'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
compensatedSigma = coeffvalues(fitresult);
compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar
compensatedOptimum = compensatedSigma(2) .* 1e6

%% TEST AREA
compEmittance(1:3564) = NaN;
compOptima(1:3564) = NaN;
cLight = 299792458;
config = ConfigForIP5();

distS.B1 = DistributionFromProfileData( profiles.B1.timeref.*cLight, profiles.B1.bunch(1,:), 'normalize');
distS.B2 = DistributionFromProfileData(-profiles.B2.timeref.*cLight, profiles.B2.bunch(1,:), 'normalize');

for bunch=fill5277.data{1}.meta.colliding.b1

theLumi = fill5277.data{1}.h.bunchLumi(:,bunch);
theBump = fill5277.data{1}.h.bump.*1e-3;

analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.h, max(theLumi));
fitresult=fit(theBump, theLumi, fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
    'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
compensatedSigma = coeffvalues(fitresult);
compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar;
compensatedOptimum = compensatedSigma(2) .* 1e6;
compEmittance(bunch) = compensatedEmittance;
compOptima(bunch) = compensatedOptimum;
fprintf('done bunch %d - em=%.1f opt=%.1f\n', bunch, compensatedEmittance, compensatedOptimum)
end

%% DEPRECATED STUFF
%{

OLD STUFF

ref_bunch_offset = 690000; %samples
ref_bunch_len = 150; %samples
ref_bunch_padding = 40; %samples per side, for baseline correction
ref_range = (ref_bunch_offset):(ref_bunch_offset+ref_bunch_len);
bunch_B1_Bef = prof_B1_Bef(ref_range);
bunch_B2_Bef = prof_B2_Bef(ref_range+136);
bunch_B1_Aft = prof_B1_Aft(ref_range);
bunch_B2_Aft = prof_B2_Aft(ref_range+136);
bunch_timeref = (0:ref_bunch_len).*(1/40e9).*1e9;
bunch_timeref_centered = bunch_timeref - 1.834;

bunch_B1_Bef = bunch_B1_Bef - mean(bunch_B1_Bef([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B2_Bef = bunch_B2_Bef - mean(bunch_B2_Bef([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B1_Aft = bunch_B1_Aft - mean(bunch_B1_Aft([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B2_Aft = bunch_B2_Aft - mean(bunch_B2_Aft([1:ref_bunch_padding (end-ref_bunch_padding):end]));


%% simulated scans
sim.scan.gauss=[8.30414420728e+28,1.19058302237e+29,1.67912954975e+29,2.32870486051e+29,3.17468621746e+29,4.2530285703e+29,5.5971349531e+29,7.23377889498e+29,9.17830122196e+29,1.14295250201e+30,1.39650308647e+30,1.6737554856e+30,1.96732869084e+30,2.26727544225e+30,2.56147712332e+30,2.83635866689e+30,3.07788720249e+30,3.27275999763e+30,3.40963743754e+30,3.48025413684e+30,3.48025413684e+30,3.40963743754e+30,3.27275999763e+30,3.07788720249e+30,2.83635866689e+30,2.56147712332e+30,2.26727544225e+30,1.96732869084e+30,1.6737554856e+30,1.39650308647e+30,1.14295250201e+30,9.17830122196e+29,7.23377889498e+29,5.5971349531e+29,4.2530285703e+29,3.17468621746e+29,2.32870486051e+29,1.67912954975e+29,1.19058302237e+29,8.30414420728e+28]
sim.scan.init=[3.87951491914e+28,6.08375941258e+28,9.343299224e+28,1.4045649964e+29,2.06576478147e+29,2.97101774378e+29,4.17647285854e+29,5.73590824047e+29,7.69329495524e+29,1.00739013758e+30,1.28749592554e+30,1.60573001587e+30,1.95395973279e+30,2.31968547674e+30,2.68644908338e+30,3.03486751219e+30,3.34424886632e+30,3.59461836094e+30,3.76887114911e+30,3.85471749014e+30,3.84611164051e+30,3.74394542869e+30,3.55590924238e+30,3.29555002986e+30,2.98067415582e+30,2.63134243011e+30,2.26776694938e+30,1.90842200689e+30,1.56861586962e+30,1.25965553181e+30,9.88611389907e+29,7.58589920658e+29,5.69367129559e+29,4.18220534642e+29,3.00810316084e+29,2.11989938034e+29,1.46465863692e+29,9.92686615865e+28,6.60364501136e+28,4.31394109349e+28]
sim.scan.end=[4.39241220637e+28,6.82389917755e+28,1.03719340978e+29,1.54162774338e+29,2.23985186939e+29,3.18003292752e+29,4.41051127129e+29,5.97424035089e+29,7.90180292212e+29,1.02037306044e+30,1.28632170763e+30,1.58305033229e+30,1.9020175832e+30,2.23123705738e+30,2.55584500322e+30,2.85911300722e+30,3.12383003525e+30,3.33390147231e+30,3.47595438732e+30,3.54072094012e+30,3.52400396385e+30,3.42709771884e+30,3.2566191198e+30,3.02378259237e+30,2.74321975412e+30,2.43150234672e+30,2.10556587037e+30,1.78123904674e+30,1.47205344773e+30,1.18844645713e+30,9.37398645889e+29,7.22481944968e+29,5.44246278723e+29,4.00840286913e+29,2.88746230815e+29,2.03512142037e+29,1.40386593362e+29,9.47983327721e+28,6.26639150538e+28,4.05400370908e+28]

sim.bump=linspace(-40e-6,40e-6,40).*2


%% fake bunchlen

dataBefore_fakedLen = dataBefore;
dataAfter_fakedLen = dataAfter;

dataBefore_fakedLen.h.bunchLength(:) = 0.07;
dataAfter_fakedLen.h.bunchLength(:) = 0.07;

[resultBef_fakedLen,errorsBef_fakedLen] = BunchEmittancesFromScan(config, dataBefore_fakedLen);
[resultAft_fakedLen,errorsAft_fakedLen] = BunchEmittancesFromScan(config, dataAfter_fakedLen);


%}
