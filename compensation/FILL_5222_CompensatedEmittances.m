clear
get = @(i,s) s(i);
fill = 5222;

%% scans
config = ConfigForIP5();
data = ExtractEmittanceScans(config, fill);

scan = 1;
%% process
fprintf('Dealing with scan %d\n', scan);
[result,errors] = BunchEmittancesFromScan(config, data{scan});

%% profiles
profiles.B1.raw = h5read('scope_profiles_5222/PROFILE_B1_b1_20160819174110.h5','/Profile/deconvoluted');
profiles.B2.raw = h5read('scope_profiles_5222/PROFILE_B2_b1_20160819174131.h5','/Profile/deconvoluted');
%profiles.B1.raw = mean(h5read('scope_profiles_5222/PROFILE_B1_b1_20160819174110.h5','/Profile/profile'),1);
%profiles.B2.raw = mean(h5read('scope_profiles_5222/PROFILE_B2_b1_20160819174131.h5','/Profile/profile'),1);

%% split bunches
profiles.scope.B1 = struct('samplerate', 40e9, 'fRF', 400.79e6+0.00069e6, 'sampleLag', -35307);
profiles.scope.B2 = struct('samplerate', 40e9, 'fRF', 400.79e6-0.00010e6, 'sampleLag', -35501);

[profiles.B1.timeref, profiles.B1.bunch, profiles.scope.B1] = SplitScopeAcqToBunches(profiles.scope.B1, profiles.B1.raw, 5222, 1); 
[profiles.B2.timeref, profiles.B2.bunch, profiles.scope.B2] = SplitScopeAcqToBunches(profiles.scope.B2, profiles.B2.raw, 5222, 2); 
%%
%{
profiles.B1.bunch(:,50:end-2) = fliplr(profiles.B1.bunch(:,1:49));
profiles.B2.bunch(:,50:end-2) = fliplr(profiles.B2.bunch(:,1:49));
profiles.B1.bunch(:,end) = 0;
profiles.B2.bunch(:,end) = 0;
profiles.B1.bunch(profiles.B1.bunch < 0.01) = 0;
profiles.B2.bunch(profiles.B2.bunch < 0.01) = 0;
%}
%%
compEmittance(1:3564) = NaN;
compOptima(1:3564) = NaN;
cLight = 299792458;

for bunch=data{scan}.meta.colliding.b1
    bunchIdx.B1 = find(data{scan}.meta.filledSlots.b1==bunch);
    bunchIdx.B2 = find(data{scan}.meta.filledSlots.b2==bunch);
    fprintf('dealing with bunch %d = filled index %d / %d\n', bunch, bunchIdx.B1, bunchIdx.B2);
    distS.B1 = DistributionFromProfileData( profiles.B1.timeref.*cLight, profiles.B1.bunch(bunchIdx.B1,:), 'normalize');
    distS.B2 = DistributionFromProfileData( profiles.B2.timeref.*cLight, profiles.B2.bunch(bunchIdx.B2,:), 'normalize');
    
    theLumi = data{scan}.h.bunchLumi(:,bunch);
    theBump = data{scan}.h.bump.*1e-3;
    
%%
    analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.h, max(theLumi));
    fitresult=fit(theBump, theLumi, fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
        'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
    compensatedSigma = coeffvalues(fitresult);
    compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar;
    compensatedOptimum = compensatedSigma(2) .* 1e6;
    compEmittance(bunch) = compensatedEmittance;
    compOptima(bunch) = compensatedOptimum;
    fprintf('done bunch %d - em=%.1f opt=%.1f\n', bunch, compensatedEmittance, compensatedOptimum)
end
%% plot
datestamp = java.util.Date(data{scan}.raw.bunchLumi(1,1));
fig = figure;
set(fig, 'Position', [100 100 1000 500])
subplot(2,1,1)
hold all
plot([result.h.emittance], 'r')
plot(compEmittance, 'k')
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')
xlim([0 3565])
xlabel('Bunch Number [25ns slots]');
ylabel('Norm. Emittance [um]')
title(sprintf('Fill %d - Emittance Scan #%d @ %s - HORIZONTAL/CROSSING', fill, scan, char(datestamp.toString())));
ylim([0 4])
legend('Gaussian / FWHM', 'Deconvoluted longitudinal profile')


subplot(2,1,2)
plot([result.v.emittance], 'k')
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')
xlim([0 3565])
xlabel('Bunch Number [25ns slots]');
ylabel('Norm. Emittance [um]')
ylim([0 4])
title(sprintf('Fill %d - Emittance Scan #%d @ %s - VERTICAL/SEPARATION', fill, scan, char(datestamp.toString())));

%% export
compEmittanceH = compEmittance;
uncompEmittanceH = [result.h.emittance];
emittanceV = [result.v.emittance];
scanTime = char(datestamp.toString());
scanTimeStamp = data{scan}.raw.bunchLumi(1,1);

save(sprintf('Emittances_Fill%d_Scan%d.mat', fill ,scan), 'compEmittanceH',  'uncompEmittanceH', 'emittanceV', 'scanTime', 'scanTimeStamp');

