%#ok<*SAGROW>
clear
load('testScriptData');

config.crossingAngles.h = 185e-6 * 2;
config.crossingAngles.v = 0;
config.energy = 6500;
config.betaStar = 0.4;

sigma.s = 0.077;
emittance.h = 2.5E-6;
emittance.v = 3.0E-6;
intensity = 1.015E11;

cLight = 299792458;
frev = 11245;
mass = 0.9382720;
limits.h = 10;
limits.v = 10;
limits.s = 10;


gamma = 1+config.energy/mass;
sigma.x = sqrt(config.betaStar*emittance.h/gamma);
sigma.y = sqrt(config.betaStar*emittance.v/gamma);

distX = @(x) normpdf(x,0,sigma.x);
distY = @(y) normpdf(y,0,sigma.y);
% Gaussian ==> distS = @(s) normpdf(s,0,sigma.s);
distS.B1 = DistributionFromProfileData(bunch_timeref.*cLight, bunch_B1, 'normalize', 'centerPeak');
distS.B2 = DistributionFromProfileData(-bunch_timeref.*cLight, bunch_B2, 'normalize', 'centerPeak');


angleOfB1 = struct('h', +config.crossingAngles.h/2, 'v', +config.crossingAngles.v/2);
angleOfB2 = struct('h', -config.crossingAngles.h/2, 'v', -config.crossingAngles.v/2);

separateB1 = @(sep) struct('h', sep/2, 'v', 0, 's', 0);
separateB2 = @(sep) struct('h', -sep/2, 'v', 0, 's', 0);

boundary.x = [-limits.h*sigma.x, limits.h*sigma.x];
boundary.y = [-limits.v*sigma.y, limits.v*sigma.y];
boundary.s = [-limits.s*sigma.s, limits.s*sigma.s];
boundary.s0 = [-limits.s*sigma.s, limits.s*sigma.s];


sim.bump = linspace(-100e-6, 100e-6, 40);
for i=1:numel(sim.bump)
    overlap = Overlap( ...
        Density(distX, distY, distS.B1, config.betaStar, angleOfB1, separateB1(sim.bump(i)), 'hourglass'), ...
        Density(distX, distY, distS.B2, config.betaStar, angleOfB2, separateB2(sim.bump(i)), 'hourglass'));

    tic
    % 1e-4 for m^2 -> cm^2
    sim.scan(i) = 2 * 1e-4 * cos(config.crossingAngles.h)^2 * cos(config.crossingAngles.v)^2 ...
        * Integral4D(overlap,40,boundary.x,boundary.y,boundary.s,boundary.s0) ...
        * frev * intensity^2; 
    toc
end