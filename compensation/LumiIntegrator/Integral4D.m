function [ result ] = Integral4D( func, pointsPerDimension, bound1, bound2, bound3, bound4)
%INTEGRAL4D calculates the 4D integral of the given function
%   Uses fast but approximative (rectangular) integration
    steps1 = linspace(bound1(1),bound1(2),pointsPerDimension);
    steps2 = linspace(bound2(1),bound2(2),pointsPerDimension);
    steps3 = linspace(bound3(1),bound3(2),pointsPerDimension);
    steps4 = linspace(bound4(1),bound4(2),pointsPerDimension);
    stepSize1 = mean(diff(steps1));
    stepSize2 = mean(diff(steps2));
    stepSize3 = mean(diff(steps3));
    stepSize4 = mean(diff(steps4));
    
    [g1,g2,g3,g4] = ndgrid(steps1, steps2, steps3, steps4);

     result = sum(func(g1(:),g2(:),g3(:),g4(:))) .* (stepSize1 * stepSize2 * stepSize3 * stepSize4); 
end

