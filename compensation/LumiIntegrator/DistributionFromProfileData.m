function [ calculator ] = DistributionFromProfileData( x, y, varargin )
%DISTRIBUTIONFROMPROFILEDATA Generate a normalized distribution from data
%   units of x must be meters. y must be dimensionless and normalized.
%   additional options:
%       - 'normalize' to normalize the integral to 1 (if not already the case)
%       - 'centerIntegral' to center the expected value of the distribution
%       - 'centerPeak' to center the peak of the distribution

dist_normFactor = 1;
dist_xOffset = 0;
    function [ dist ] = calculateDist(xq)
        dist = interp1(x-dist_xOffset, y, xq) ./ dist_normFactor;
        dist(isnan(dist)) = 0;
    end

if any(ismember(varargin,'normalize'))
    dist_normFactor = integral(@calculateDist, min(x), max(x));
    fprintf('Normalizing distribution, norm. factor = 1/%.4f\n', dist_normFactor);
end

if any(ismember(varargin,'centerIntegral'))
    dist_xOffset = integral(@(s) calculateDist(s).*s, min(x), max(x));
    fprintf('Centering distribution (integral), x offset = %.4f\n', dist_xOffset);
end

if any(ismember(varargin,'centerPeak'))
    [~,index] = max(y);
    dist_xOffset = x(index);
    fprintf('Centering distribution (peak), x offset = %.4f\n', dist_xOffset);
end


calculator = @calculateDist;

end

