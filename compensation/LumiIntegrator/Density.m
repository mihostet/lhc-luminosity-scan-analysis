function [ densityCalculator ] = Density( densityX, densityY, densityS, betaStar, crossingAngles, offset, varargin )
%DENSITY Build a total density calculator function
%   Builds a total density calculator function for the passed-in
%   distribution functions densityX, densityY, densityS. Crossing angles
%   and separations are taken into account here.

    sinXingX = sin(crossingAngles.h);
    sinXingY = sin(crossingAngles.v);
    cosXingX = cos(crossingAngles.h);
    cosXingY = cos(crossingAngles.v);
    
    if any(ismember(varargin,'hourglass'))
        hourglassFactor = @(x,y,s,s0) 1./sqrt((1.0+(s./betaStar).^2));
    else
        hourglassFactor = @(x,y,s,s0) 1;
    end
    
    getX = @(x,y,s,s0) x.*cosXingX - s.*sinXingX + offset.h;
    getY = @(x,y,s,s0) y.*cosXingY - s.*sinXingY + offset.v;
    getS = @(x,y,s,s0) (s-s0+offset.s) .* (cosXingX * cosXingY); % x*sinXingX, y*sinXingY neglegted 
    
    function [density] = calculateDensity(x,y,s,s0)
        hg = hourglassFactor(x,y,s,s0);
        density = (hg.^2) .* ...
            densityX(getX(x,y,s,s0) .* hg) .* ...
            densityY(getY(x,y,s,s0) .* hg) .* ...
            densityS(getS(x,y,s,s0));
    end
    densityCalculator = @calculateDensity;
end

