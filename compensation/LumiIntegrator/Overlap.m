function [ overlapDensityCalculator ] = Overlap( densityBeam1, densityBeam2 )
%OVERLAP Returns an overlap density calculator for the two beam density functions
    overlapDensityCalculator = @(x,y,s,s0) (densityBeam1(x,y,s,s0) .* densityBeam2(x,y,s,-s0));
end

