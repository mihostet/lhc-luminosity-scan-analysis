clear

load('cms_offline_5024')
%% extract
config = ConfigForIP5();
data = ExtractEmittanceScansFromOffline(config, 5024, cms_offline_5024);
%% calc
dataBefore = data{2};
dataAfter = data{4};

[resultBef,errorsBef] = BunchEmittancesFromScan(config, dataBefore);
[resultAft,errorsAft] = BunchEmittancesFromScan(config, dataAfter);
%% getter
get = @(i,s) s(i);

%% long profiles
prof_B1_Bef=h5read('profiles_fill_5024/PROFILE_B1_b1_20160617085542.h5','/Profile/deconvoluted');
prof_B2_Bef=h5read('profiles_fill_5024/PROFILE_B2_b1_20160617085703.h5','/Profile/deconvoluted');
prof_B1_Aft=h5read('profiles_fill_5024/PROFILE_B1_b1_20160617101610.h5','/Profile/deconvoluted');
prof_B2_Aft=h5read('profiles_fill_5024/PROFILE_B2_b1_20160617101710.h5','/Profile/deconvoluted');

%% split bunches
scope.B1 = struct('samplerate', 40e9, 'fRF', 400.79e6+0.0007e6);
scope.B2 = struct('samplerate', 40e9, 'fRF', 400.79e6-0.00015e6);

[bunch_B1_Bef_timeref, bunch_B1_Bef, scope.B1] = SplitScopeAcqToBunches(scope.B1, prof_B1_Bef, 5024, 1); 
[bunch_B2_Bef_timeref, bunch_B2_Bef, scope.B2] = SplitScopeAcqToBunches(scope.B2, prof_B2_Bef, 5024, 2); 
[bunch_B1_Aft_timeref, bunch_B1_Aft, scope.B1] = SplitScopeAcqToBunches(scope.B1, prof_B1_Aft, 5024, 1); 
[bunch_B2_Aft_timeref, bunch_B2_Aft, scope.B2] = SplitScopeAcqToBunches(scope.B2, prof_B2_Aft, 5024, 2); 

%% intensities

dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
dataMiner.setStartTimestamp(dataBefore.raw.totalLumi(1,1))
dataMiner.setEndTimestamp(dataAfter.raw.totalLumi(end,1))
bunchIntB1_fbct = dataMiner.fetchDataSetTable('LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
bunchIntB2_fbct = dataMiner.fetchDataSetTable('LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY');

%% filleds
filledslots_B1 = FilledSlotsFromFBCT(bunchIntB1_fbct(1,:));
filledslots_B2 = FilledSlotsFromFBCT(bunchIntB2_fbct(1,:));

%% batch
for b=1:numel(dataBefore.meta.colliding.b1)
    fprintf('=== dealing with bunch %d of %d === \n', b, numel(dataBefore.meta.colliding.b1));
    emittances = struct('h',get(dataBefore.meta.colliding.b1(b),[resultBef.h.emittance]),'v',get(dataBefore.meta.colliding.b1(b),[resultBef.v.emittance]));
    bunchIntBefore = 0.5 * (bunchIntB1_fbct(1,dataBefore.meta.colliding.b1(b)+1) ...
        + bunchIntB2_fbct(1,dataBefore.meta.colliding.b2(b)+1))
    bunchIntAfter = 0.5 * (bunchIntB1_fbct(end,dataBefore.meta.colliding.b1(b)+1) ...
        + bunchIntB2_fbct(end,dataBefore.meta.colliding.b2(b)+1))
    %% sim before
    sim{b}.before = LumiForBunchProfile(bunch_B1_Bef_timeref, bunch_B1_Bef(100,:), bunch_B2_Bef_timeref, bunch_B2_Bef(100,:), ...
        emittances, bunchIntBefore);
    %% sim after
    sim{b}.after = LumiForBunchProfile(bunch_B1_Aft_timeref, bunch_B1_Aft(100,:), bunch_B2_Aft_timeref, bunch_B2_Aft(100,:), ...
        emittances, bunchIntAfter);
end







%% DEPRECATED STUFF
%{

OLD STUFF

ref_bunch_offset = 690000; %samples
ref_bunch_len = 150; %samples
ref_bunch_padding = 40; %samples per side, for baseline correction
ref_range = (ref_bunch_offset):(ref_bunch_offset+ref_bunch_len);
bunch_B1_Bef = prof_B1_Bef(ref_range);
bunch_B2_Bef = prof_B2_Bef(ref_range+136);
bunch_B1_Aft = prof_B1_Aft(ref_range);
bunch_B2_Aft = prof_B2_Aft(ref_range+136);
bunch_timeref = (0:ref_bunch_len).*(1/40e9).*1e9;
bunch_timeref_centered = bunch_timeref - 1.834;

bunch_B1_Bef = bunch_B1_Bef - mean(bunch_B1_Bef([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B2_Bef = bunch_B2_Bef - mean(bunch_B2_Bef([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B1_Aft = bunch_B1_Aft - mean(bunch_B1_Aft([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B2_Aft = bunch_B2_Aft - mean(bunch_B2_Aft([1:ref_bunch_padding (end-ref_bunch_padding):end]));


%% simulated scans
sim.scan.gauss=[8.30414420728e+28,1.19058302237e+29,1.67912954975e+29,2.32870486051e+29,3.17468621746e+29,4.2530285703e+29,5.5971349531e+29,7.23377889498e+29,9.17830122196e+29,1.14295250201e+30,1.39650308647e+30,1.6737554856e+30,1.96732869084e+30,2.26727544225e+30,2.56147712332e+30,2.83635866689e+30,3.07788720249e+30,3.27275999763e+30,3.40963743754e+30,3.48025413684e+30,3.48025413684e+30,3.40963743754e+30,3.27275999763e+30,3.07788720249e+30,2.83635866689e+30,2.56147712332e+30,2.26727544225e+30,1.96732869084e+30,1.6737554856e+30,1.39650308647e+30,1.14295250201e+30,9.17830122196e+29,7.23377889498e+29,5.5971349531e+29,4.2530285703e+29,3.17468621746e+29,2.32870486051e+29,1.67912954975e+29,1.19058302237e+29,8.30414420728e+28]
sim.scan.init=[3.87951491914e+28,6.08375941258e+28,9.343299224e+28,1.4045649964e+29,2.06576478147e+29,2.97101774378e+29,4.17647285854e+29,5.73590824047e+29,7.69329495524e+29,1.00739013758e+30,1.28749592554e+30,1.60573001587e+30,1.95395973279e+30,2.31968547674e+30,2.68644908338e+30,3.03486751219e+30,3.34424886632e+30,3.59461836094e+30,3.76887114911e+30,3.85471749014e+30,3.84611164051e+30,3.74394542869e+30,3.55590924238e+30,3.29555002986e+30,2.98067415582e+30,2.63134243011e+30,2.26776694938e+30,1.90842200689e+30,1.56861586962e+30,1.25965553181e+30,9.88611389907e+29,7.58589920658e+29,5.69367129559e+29,4.18220534642e+29,3.00810316084e+29,2.11989938034e+29,1.46465863692e+29,9.92686615865e+28,6.60364501136e+28,4.31394109349e+28]
sim.scan.end=[4.39241220637e+28,6.82389917755e+28,1.03719340978e+29,1.54162774338e+29,2.23985186939e+29,3.18003292752e+29,4.41051127129e+29,5.97424035089e+29,7.90180292212e+29,1.02037306044e+30,1.28632170763e+30,1.58305033229e+30,1.9020175832e+30,2.23123705738e+30,2.55584500322e+30,2.85911300722e+30,3.12383003525e+30,3.33390147231e+30,3.47595438732e+30,3.54072094012e+30,3.52400396385e+30,3.42709771884e+30,3.2566191198e+30,3.02378259237e+30,2.74321975412e+30,2.43150234672e+30,2.10556587037e+30,1.78123904674e+30,1.47205344773e+30,1.18844645713e+30,9.37398645889e+29,7.22481944968e+29,5.44246278723e+29,4.00840286913e+29,2.88746230815e+29,2.03512142037e+29,1.40386593362e+29,9.47983327721e+28,6.26639150538e+28,4.05400370908e+28]

sim.bump=linspace(-40e-6,40e-6,40).*2


%% fake bunchlen

dataBefore_fakedLen = dataBefore;
dataAfter_fakedLen = dataAfter;

dataBefore_fakedLen.h.bunchLength(:) = 0.07;
dataAfter_fakedLen.h.bunchLength(:) = 0.07;

[resultBef_fakedLen,errorsBef_fakedLen] = BunchEmittancesFromScan(config, dataBefore_fakedLen);
[resultAft_fakedLen,errorsAft_fakedLen] = BunchEmittancesFromScan(config, dataAfter_fakedLen);


%}
