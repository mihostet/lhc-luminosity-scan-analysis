%#ok<*SAGROW>
clear

config.crossingAngles.h = 185e-6 * 2;
%config.crossingAngles.h = 145e-6 * 2;
config.crossingAngles.v = 0;
config.energy = 6500;
config.betaStar = 0.4;
%config.betaStar = 0.8;

sigma.s = 0.075;
emittance.h = 3.5E-6;
emittance.v = 3.0E-6;
intensity = 1.0E11;

cLight = 299792458;
frev = 11245;
mass = 0.9382720;
limits.h = 5;
limits.v = 5;
limits.s = 5;


gamma = 1+config.energy/mass;
sigma.x = sqrt(config.betaStar*emittance.h/gamma);
sigma.y = sqrt(config.betaStar*emittance.v/gamma);

distX = @(x) normpdf(x,0,sigma.x); %distribution(sigma.x,'cos2');% @(x) normpdf(x,0,sigma.x);
distY = @(y) normpdf(y,0,sigma.y);% NO EFFECT (as expected, sep plane) %@(y) normpdf(y,0,sigma.y);
% Gaussian ==> distS = @(s) normpdf(s,0,sigma.s);
distS.B1 = distribution(sigma.s,'cos2');%@(s) normpdf(s,0,sigma.s);  %distribution(sigma.s,'rect'); %@(s) normpdf(s,0,sigma.s); %distribution(sigma.s,'cos2'); %@(s) normpdf(s,0,sigma.s); %DistributionFromProfileData(bunch_timeref.*cLight, bunch_B1, 'normalize', 'centerPeak');
distS.B2 = distS.B1; %DistributionFromProfileData(-bunch_timeref.*cLight, bunch_B2, 'normalize', 'centerPeak');


angleOfB1 = struct('h', +config.crossingAngles.h/2, 'v', +config.crossingAngles.v/2);
angleOfB2 = struct('h', -config.crossingAngles.h/2, 'v', -config.crossingAngles.v/2);

separateB1 = @(sep) struct('h',  sep/2, 'v', 0, 's', 0);
separateB2 = @(sep) struct('h',  -sep/2, 'v', 0, 's', 0);

boundary.x = [-limits.h*sigma.x, limits.h*sigma.x];
boundary.y = [-limits.v*sigma.y, limits.v*sigma.y];
boundary.s = [-limits.s*sigma.s, limits.s*sigma.s];
boundary.s0 = [-limits.s*sigma.s, limits.s*sigma.s];


sim.bump = linspace(-100e-6, 100e-6, 61);
for i=1:numel(sim.bump)
    density.B1 = Density(distX, distY, distS.B1, config.betaStar, angleOfB1, separateB1(sim.bump(i)), '');
    density.B2 = Density(distX, distY, distS.B2, config.betaStar, angleOfB2, separateB2(sim.bump(i)), ''); % hourglass
    overlap = Overlap(density.B1, density.B2);

    tic
    % 1e-4 for m^2 -> cm^2
    sim.scan(i) = 2 * 1e-4 * cos(config.crossingAngles.h)^2 * cos(config.crossingAngles.v)^2 ...
        * Integral4D(overlap,50,boundary.x,boundary.y,boundary.s,boundary.s0) ...
        * frev * intensity^2; 
    toc
end

%%
analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.h, max([sim.scan]));
fitresult=fit([sim.bump]', [sim.scan]', fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
    'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
compensatedSigma = coeffvalues(fitresult);
compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar
compensatedOptimum = compensatedSigma(2) .* 1e6


%%
gaussianResult = fit([sim.bump]', [sim.scan]', fittype('gauss1'));
fitvalues = coeffvalues(gaussianResult);
gaussianFitSigma = fitvalues(3) / 2;
gaussianSigma = sqrt(gaussianFitSigma^2 - sigma.s^2*sin(config.crossingAngles.h/2)^2) / cos(config.crossingAngles.h/2);
gaussianEmittance = 1e6 * (gaussianSigma)^2 * (config.energy/0.938) / config.betaStar
gaussianOptimum = fitvalues(2) .* 1e6

%%
ss=linspace(-limits.s*sigma.s, limits.s*sigma.s, 501);
fwhmBunchLen = fwhm(ss, distS.B1(ss)) / (2*sqrt(2*log(2)))
fwhmSigma = sqrt(gaussianFitSigma^2 - fwhmBunchLen^2*sin(config.crossingAngles.h/2)^2) / cos(config.crossingAngles.h/2);
fwhmEmittance = 1e6 * (fwhmSigma)^2 * (config.energy/0.938) / config.betaStar
fwhmOptimum = fitvalues(2) .* 1e6

%%
ds = linspace(-sigma.s*10,sigma.s*10,200);
fprintf('True RMS of distributions (ref = %.5f m): \n  B1=%.5f m\n  B2=%.5f m\n', ...
    sigma.s, ...
    sqrt(trapz(ds, distS.B1(ds).*ds.^2)), ...
    sqrt(trapz(ds, distS.B2(ds).*ds.^2)));
clear ds
