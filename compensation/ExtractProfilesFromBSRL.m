function [ timeref, bunchB1, bunchB2 ] = ExtractProfilesFromBSRL( fill, point )
%% mine data
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
dataMiner.setFill(fill, 'STABLE');

dataMiner.setStartTimestamp(point);
dataMiner.setEndTimestamp(point+5*60e3);

hist_B1=BSRLCorrectHistogram(dataMiner.fetchDataSetTable('BSRL.B1.SYS_A:HIST'), ...
    dataMiner.fetchDataSetTable('BSRL.B1.SYS_A:HIST_AGE_REV','BSRL.B1.SYS_A:HIST'));

hist_B2=BSRLCorrectHistogram(dataMiner.fetchDataSetTable('BSRL.B2.SYS_A:HIST'), ...
    dataMiner.fetchDataSetTable('BSRL.B2.SYS_A:HIST_AGE_REV','BSRL.B2.SYS_A:HIST'));

[timeref, bunchB1] = BSRLSplitBunches(hist_B1, fill, 1);
[~, bunchB2] = BSRLSplitBunches(hist_B2, fill, 2);

