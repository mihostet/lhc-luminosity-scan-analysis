function [ lambda ] = distribution( rms, type )
%DISTTEST Summary of this function goes here
%   Detailed explanation goes here

cosSquarePeriod = rms * 2.7661594070;
    function [out] = cosSquare(x)
        x(x>cosSquarePeriod) = cosSquarePeriod;
        x(x<-cosSquarePeriod) = -cosSquarePeriod;
        
        out = cos(x.*(0.5*pi/cosSquarePeriod)).^2 / cosSquarePeriod;
    end

rectangularBounds = rms / 0.5773503574;
    function [out] = rectangular(x)
        out = zeros(size(x));
        out(x>-rectangularBounds & x<rectangularBounds) = 1/(2*rectangularBounds);
    end


if strcmp(type, 'cos2')
    lambda = @cosSquare;
end
if strcmp(type, 'rect')
    lambda = @rectangular;
end

end

