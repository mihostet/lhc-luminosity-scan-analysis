clear
get = @(i,s) s(i);
fill = 5339;

%% scans
config = ConfigForIP5();
config.crossingAngles.h = 140e-6 * 2;
data = ExtractEmittanceScans(config, fill);
%% extract profiles
%{
for scan=1:numel(data)
    fprintf('Extracting long. profiles for scan %d\n', scan);
    [bsrl_profiles.timeref, bsrl_profiles.B1{scan}, bsrl_profiles.B2{scan}] = ExtractProfilesFromBSRL(fill, data{scan}.raw.bunchLumi(1,1));
end
%}
for scan = 1:numel(data)
%% process
fprintf('Dealing with scan %d\n', scan);
[result,errors] = BunchEmittancesFromScan(config, data{scan});



%%
compEmittance(1:3564) = NaN;
compOptima(1:3564) = NaN;
cLight = 299792458;
load('5246/profiles5246.mat');
profiles.B1.bunch(1,50:end-2)=fliplr(profiles.B1.bunch(1,1:49));
profiles.B2.bunch(1,50:end-2)=fliplr(profiles.B2.bunch(1,1:49));
%%
distS.B1 = DistributionFromProfileData( profiles.B1.timeref.*cLight, profiles.B1.bunch(1,:), 'normalize', 'centerPeak');
distS.B2 = DistributionFromProfileData(-profiles.B2.timeref.*cLight, profiles.B2.bunch(1,:), 'normalize', 'centerPeak');

for bunch=data{scan}.meta.colliding.b1
    bunchIdx.B1 = find(data{scan}.meta.filledSlots.b1==bunch);
    bunchIdx.B2 = find(data{scan}.meta.filledSlots.b2==bunch);
    fprintf('dealing with bunch %d = filled index %d / %d\n', bunch, bunchIdx.B1, bunchIdx.B2);
%    distS.B1 = DistributionFromProfileData( bsrl_profiles.timeref.*cLight, bsrl_profiles.B1{scan}(bunchIdx.B1,:), 'normalize', 'centerIntegral');
%    distS.B2 = DistributionFromProfileData( bsrl_profiles.timeref.*cLight, bsrl_profiles.B2{scan}(bunchIdx.B2,:), 'normalize', 'centerIntegral');
    
    theLumi = data{scan}.h.bunchLumi(:,bunch);
    theBump = data{scan}.h.bump.*1e-3;
    
%%
    analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.h, max(theLumi));
    fitresult=fit(theBump, theLumi, fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
        'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
    compensatedSigma = coeffvalues(fitresult);
    compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar;
    compensatedOptimum = compensatedSigma(2) .* 1e6;
    compEmittance(bunch) = compensatedEmittance;
    compOptima(bunch) = compensatedOptimum;
    fprintf('done bunch %d - em=%.1f opt=%.1f\n', bunch, compensatedEmittance, compensatedOptimum)
end
%% plot
datestamp = java.util.Date(data{scan}.raw.bunchLumi(1,1));
fig = figure;
set(fig, 'Position', [100 100 1000 500])
subplot(2,1,1)
hold all
plot([result.h.emittance], 'r')
plot(compEmittance, 'k')
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')
xlim([0 3565])
xlabel('Bunch Number [25ns slots]');
ylabel('Norm. Emittance [um]')
title(sprintf('Fill %d - Emittance Scan #%d @ %s - HORIZONTAL/CROSSING', fill, scan, char(datestamp.toString())));
ylim([0 4])
legend('Gaussian / FWHM', 'Deconvoluted longitudinal profile')


subplot(2,1,2)
plot([result.v.emittance], 'k')
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')
xlim([0 3565])
xlabel('Bunch Number [25ns slots]');
ylabel('Norm. Emittance [um]')
ylim([0 4])
title(sprintf('Fill %d - Emittance Scan #%d @ %s - VERTICAL/SEPARATION', fill, scan, char(datestamp.toString())));

%% export
compEmittanceH = compEmittance;
uncompEmittanceH = [result.h.emittance];
emittanceV = [result.v.emittance];
scanTime = char(datestamp.toString());
scanTimeStamp = data{scan}.raw.bunchLumi(1,1);

save(sprintf('Emittances_Fill%d_Scan%d.mat', fill ,scan), 'compEmittanceH',  'uncompEmittanceH', 'emittanceV', 'scanTime', 'scanTimeStamp');


end
%% DEPRECATED STUFF
%{

OLD STUFF

ref_bunch_offset = 690000; %samples
ref_bunch_len = 150; %samples
ref_bunch_padding = 40; %samples per side, for baseline correction
ref_range = (ref_bunch_offset):(ref_bunch_offset+ref_bunch_len);
bunch_B1_Bef = prof_B1_Bef(ref_range);
bunch_B2_Bef = prof_B2_Bef(ref_range+136);
bunch_B1_Aft = prof_B1_Aft(ref_range);
bunch_B2_Aft = prof_B2_Aft(ref_range+136);
bunch_timeref = (0:ref_bunch_len).*(1/40e9).*1e9;
bunch_timeref_centered = bunch_timeref - 1.834;

bunch_B1_Bef = bunch_B1_Bef - mean(bunch_B1_Bef([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B2_Bef = bunch_B2_Bef - mean(bunch_B2_Bef([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B1_Aft = bunch_B1_Aft - mean(bunch_B1_Aft([1:ref_bunch_padding (end-ref_bunch_padding):end]));
bunch_B2_Aft = bunch_B2_Aft - mean(bunch_B2_Aft([1:ref_bunch_padding (end-ref_bunch_padding):end]));


%% simulated scans
sim.scan.gauss=[8.30414420728e+28,1.19058302237e+29,1.67912954975e+29,2.32870486051e+29,3.17468621746e+29,4.2530285703e+29,5.5971349531e+29,7.23377889498e+29,9.17830122196e+29,1.14295250201e+30,1.39650308647e+30,1.6737554856e+30,1.96732869084e+30,2.26727544225e+30,2.56147712332e+30,2.83635866689e+30,3.07788720249e+30,3.27275999763e+30,3.40963743754e+30,3.48025413684e+30,3.48025413684e+30,3.40963743754e+30,3.27275999763e+30,3.07788720249e+30,2.83635866689e+30,2.56147712332e+30,2.26727544225e+30,1.96732869084e+30,1.6737554856e+30,1.39650308647e+30,1.14295250201e+30,9.17830122196e+29,7.23377889498e+29,5.5971349531e+29,4.2530285703e+29,3.17468621746e+29,2.32870486051e+29,1.67912954975e+29,1.19058302237e+29,8.30414420728e+28]
sim.scan.init=[3.87951491914e+28,6.08375941258e+28,9.343299224e+28,1.4045649964e+29,2.06576478147e+29,2.97101774378e+29,4.17647285854e+29,5.73590824047e+29,7.69329495524e+29,1.00739013758e+30,1.28749592554e+30,1.60573001587e+30,1.95395973279e+30,2.31968547674e+30,2.68644908338e+30,3.03486751219e+30,3.34424886632e+30,3.59461836094e+30,3.76887114911e+30,3.85471749014e+30,3.84611164051e+30,3.74394542869e+30,3.55590924238e+30,3.29555002986e+30,2.98067415582e+30,2.63134243011e+30,2.26776694938e+30,1.90842200689e+30,1.56861586962e+30,1.25965553181e+30,9.88611389907e+29,7.58589920658e+29,5.69367129559e+29,4.18220534642e+29,3.00810316084e+29,2.11989938034e+29,1.46465863692e+29,9.92686615865e+28,6.60364501136e+28,4.31394109349e+28]
sim.scan.end=[4.39241220637e+28,6.82389917755e+28,1.03719340978e+29,1.54162774338e+29,2.23985186939e+29,3.18003292752e+29,4.41051127129e+29,5.97424035089e+29,7.90180292212e+29,1.02037306044e+30,1.28632170763e+30,1.58305033229e+30,1.9020175832e+30,2.23123705738e+30,2.55584500322e+30,2.85911300722e+30,3.12383003525e+30,3.33390147231e+30,3.47595438732e+30,3.54072094012e+30,3.52400396385e+30,3.42709771884e+30,3.2566191198e+30,3.02378259237e+30,2.74321975412e+30,2.43150234672e+30,2.10556587037e+30,1.78123904674e+30,1.47205344773e+30,1.18844645713e+30,9.37398645889e+29,7.22481944968e+29,5.44246278723e+29,4.00840286913e+29,2.88746230815e+29,2.03512142037e+29,1.40386593362e+29,9.47983327721e+28,6.26639150538e+28,4.05400370908e+28]

sim.bump=linspace(-40e-6,40e-6,40).*2


%% fake bunchlen

dataBefore_fakedLen = dataBefore;
dataAfter_fakedLen = dataAfter;

dataBefore_fakedLen.h.bunchLength(:) = 0.07;
dataAfter_fakedLen.h.bunchLength(:) = 0.07;

[resultBef_fakedLen,errorsBef_fakedLen] = BunchEmittancesFromScan(config, dataBefore_fakedLen);
[resultAft_fakedLen,errorsAft_fakedLen] = BunchEmittancesFromScan(config, dataAfter_fakedLen);


%}
