function [ sim ] = LumiForBunchProfile( config, profile_B1_timeref, profile_B1, profile_B2_timeref, profile_B2, emittance, intensity, plane, transverse_profiles )
%LUMIFORBUNCHPROFILE simulates a lumi scan for a given bunch profile
%  uses IP5 config. internally swaps signs for xing angle, distribution,
%  separation of B2.
%#ok<*AGROW>


cLight = 299792458;
frev = 11245;
mass = 0.9382720;

sigma.s = 0.08; % only used for integral bounds
%emittance.h = 2.5E-6;
%emittance.v = 3.0E-6;
limits.h = 10;
limits.v = 10;
limits.s = 10;

gamma = 1+config.energy/mass;
sigma.x = sqrt(config.betaStar*emittance.h/gamma);
sigma.y = sqrt(config.betaStar*emittance.v/gamma);

if exist('transverse_profiles', 'var')
    distX.B1 = DistributionFromProfileData(transverse_profiles.B1H(:,1), transverse_profiles.B1H(:,2), 'normalize');
    distY.B1 = DistributionFromProfileData(transverse_profiles.B1V(:,1), transverse_profiles.B1V(:,2), 'normalize');
    distX.B2 = DistributionFromProfileData(transverse_profiles.B2H(:,1), transverse_profiles.B2H(:,2), 'normalize');
    distY.B2 = DistributionFromProfileData(transverse_profiles.B2V(:,1), transverse_profiles.B2V(:,2), 'normalize');
else
    distX.B1 = @(x) normpdf(x,0,sigma.x);
    distY.B1 = @(y) normpdf(y,0,sigma.y);
    distX.B2 = @(x) normpdf(x,0,sigma.x);
    distY.B2 = @(y) normpdf(y,0,sigma.y);
end

distS.B1 = DistributionFromProfileData(profile_B1_timeref.*cLight, profile_B1, 'normalize');%, 'centerPeak');
distS.B2 = DistributionFromProfileData(-profile_B2_timeref.*cLight, profile_B2, 'normalize');%, 'centerPeak');


angleOfB1 = struct('h', +config.crossingAngles.h/2, 'v', +config.crossingAngles.v/2);
angleOfB2 = struct('h', -config.crossingAngles.h/2, 'v', -config.crossingAngles.v/2);

if strcmp(plane, 'h')
    fprintf('Simulating a separation in the HORIZONTAL plane\n');
    separateB1 = @(sep) struct('h', sep/2, 'v', 0, 's', 0);
    separateB2 = @(sep) struct('h', -sep/2, 'v', 0, 's', 0);
elseif strcmp(plane, 'v')
    fprintf('Simulating a separation in the VERTICAL plane\n');
    separateB1 = @(sep) struct('h', 0, 'v', sep/2, 's', 0);
    separateB2 = @(sep) struct('h', 0, 'v', -sep/2, 's', 0);
else
    throw(MException('lumiint:badplane', sprintf('I dont know plane %s', plane)));
end

boundary.x = [-limits.h*sigma.x, limits.h*sigma.x];
boundary.y = [-limits.v*sigma.y, limits.v*sigma.y];
boundary.s = [-limits.s*sigma.s, limits.s*sigma.s];
boundary.s0 = [-limits.s*sigma.s, limits.s*sigma.s];


sim.bump = linspace(-100e-6, 100e-6, 41);
for i=1:numel(sim.bump)
    overlap = Overlap( ...
        Density(distX.B1, distY.B1, distS.B1, config.betaStar, angleOfB1, separateB1(sim.bump(i)), 'hourglass'), ...
        Density(distX.B2, distY.B2, distS.B2, config.betaStar, angleOfB2, separateB2(sim.bump(i)), 'hourglass'));

    tic
    % 1e-4 for m^2 -> cm^2
    sim.scan(i) = 2 * 1e-4 * cos(config.crossingAngles.h)^2 * cos(config.crossingAngles.v)^2 ...
        * Integral4D(overlap,41,boundary.x,boundary.y,boundary.s,boundary.s0) ...
        * frev * intensity^2; 
    toc
end


end

