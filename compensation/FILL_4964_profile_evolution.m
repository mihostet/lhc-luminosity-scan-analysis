
files_to_plot = dir('scope_profile_evolution_4964/*.h5');
figure;

Colors = jet;
NoColors = length(Colors);
Ireduced = (1:numel(files_to_plot))./(numel(files_to_plot)).*(NoColors-1)+1;
RGB = interp1(1:NoColors,Colors,Ireduced);
set(gcf,'DefaultAxesColorOrder',RGB)
clf
hold all

for h5file=files_to_plot'
    fprintf('Processing file %s\n', h5file.name);
    
    profiles.B2.raw = h5read(['scope_profile_evolution_4964/' h5file.name],'/Profile/deconvoluted');
    
    profiles.scope.B2 = struct('samplerate', 40e9, 'fRF', 400.79e6-0.00010e6, 'sampleLag', -35501);
    
    [profiles.B2.timeref, profiles.B2.bunch, profiles.scope.B2] = SplitScopeAcqToBunches(profiles.scope.B2, profiles.B2.raw, 4964, 2);
    
    plot(profiles.B2.timeref.*1e9, profiles.B2.bunch(142,:));
    
end


colormap jet
xlim([-1 1])
caxis([0 numel(files_to_plot)*0.5])
cbar=colorbar;
cbar.Label.String = 'Time in Stable Beams [h]';
box on
grid on
set(gca,'XMinorTick', 'on')
set(gca,'YMinorTick', 'on')

xlabel('Longitudinal Position [ns]');
ylabel('Amplitude [a.u.]');
