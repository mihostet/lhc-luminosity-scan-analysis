function [ lumiFunction ] = AnalyticLumi( distS_B1, distS_B2, alpha, peakLumi)
%ANALYTICLUMI Summary of this function goes here
%   Detailed explanation goes here

s = linspace(-0.5,0.5,501);
longConv = conv(distS_B1(s),distS_B2(s));
    function [ lumi ] = calculateLumi(sigma, offset, d)
        reductionIntegral = @(sep) trapz(s,longConv(1:2:end) .* exp(-s.*sin(alpha/2).*(s.*sin(alpha/2)-sep)./(sigma^2)));
        lumi = peakLumi .* exp(-(d-offset).^2./(4*sigma^2)) .* arrayfun(reductionIntegral, (d-offset)) ./ reductionIntegral(0);
    end


lumiFunction = @calculateLumi;

end

