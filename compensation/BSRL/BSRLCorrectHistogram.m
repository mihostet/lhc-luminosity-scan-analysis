function [ corrected_histogram ] = BSRLCorrectHistogram( histogram, histogram_age )
%BSRLCORRECTHISTOGRAM correct a BSRL histogram for the transfer function

bin_width = 50e-12;

bsrl_s=importdata('B1HYB_S.txt');

normalized_histogram = histogram(1,2:end) ./ histogram_age(1,2);
sampling=0:bin_width:max(bsrl_s.data(:,1));
transfer_function=spline(bsrl_s.data(:,1), bsrl_s.data(:,2), sampling);
transfer_function(1)=1;

correction = 1 - conv(normalized_histogram,1-transfer_function);

corrected_histogram = log(1./(1-normalized_histogram./correction(1:numel(1-normalized_histogram))));


end

