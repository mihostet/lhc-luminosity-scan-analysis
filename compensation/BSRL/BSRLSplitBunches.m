function [ bunchTimeref, bunches ] = BSRLSplitBunches( profile, fill, beam )
%BSRL_SPLITBUNCHES Split a BSRL histogram into individual bunches

if beam == 1
    bsrl = struct('samplerate', 20e9, 'fRF', 400.79e6+0.005e6);
else
    bsrl = struct('samplerate', 20e9, 'fRF', 400.79e6);
end
    
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
dataMiner.setFill(fill);
filledBucketsBQM = dataMiner.fetchDataSetTable(sprintf('LHC.BQM.B%d:FILLED_BUCKETS',beam));

filled = filledBucketsBQM(end-1, find(filledBucketsBQM(end-1, 2:end)>0)+1)-1;
filledSamples = round(filled.*(bsrl.samplerate/bsrl.fRF));

if ~isfield(bsrl,'sampleLag')
    pattern = zeros(bsrl.samplerate * 3564 * 25e-9, 1);
    pattern(filledSamples+1) = 1;
    [xc,lag]=xcorr(pattern, profile);
    [~,p]=max(xc);
    bsrl.sampleLag = lag(p);
    fprintf('bsrl.sampleLag not specified, guessed by cross-correlation : %d\n', bsrl.sampleLag);
end

bunchTimeref = (-25:24)./bsrl.samplerate;
bunches=zeros(numel(filledSamples), 50);
for b=1:numel(filledSamples)
bunches(b,:) = profile((filledSamples(b)-25:filledSamples(b)+24) - bsrl.sampleLag);
end

end

