function [ bunchTimeref, bunches, scope ] = SplitScopeAcqToBunches( scope, profile, fill, beam )
%SPLITSCOPEACQTOBUNCHES Splits the passes profile into individual bunches

dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
dataMiner.setFill(fill);
filledBucketsBQM = dataMiner.fetchDataSetTable(sprintf('LHC.BQM.B%d:FILLED_BUCKETS',beam));

filled = filledBucketsBQM(end-1, find(filledBucketsBQM(end-1, 2:end)>0)+1)-1;
if (isempty(filled)) 
    filled = filledBucketsBQM(end-2, find(filledBucketsBQM(end-2, 2:end)>0)+1)-1;
end
filledSamples = round(filled.*(scope.samplerate/scope.fRF));

if ~isfield(scope,'sampleLag')
    pattern = zeros(scope.samplerate * 3564 * 25e-9, 1);
    pattern(filledSamples+1) = 1;
    [xc,lag]=xcorr(pattern, profile);
    [~,p]=max(xc);
    scope.sampleLag = lag(p);
    fprintf('scope.sampleLag not specified, guessed by cross-correlation : %d\n', scope.sampleLag);
end

bunchTimeref = (-50:49)./scope.samplerate;
bunches=zeros(numel(filledSamples), 100);
for b=1:numel(filledSamples)
bunches(b,:) = profile((filledSamples(b)-50:filledSamples(b)+49) - scope.sampleLag);
end

end

