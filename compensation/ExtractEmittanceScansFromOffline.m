function [ data ] = ExtractEmittanceScansFromOffline( config, fill, offline_data )
%EXTRACTEMITTANCESCANS Extract all scans for a fill from the logging and a
%  local offline data set

%#ok<*AGROW>

    function [aligned] = alignDataToOffline(dataset, algorithm, valueForAbsent)
        interpolated = interp1(dataset(:,1), dataset(:,2:end), offline_data.timeref, algorithm);
        interpolated(isnan(interpolated)) = valueForAbsent;
        aligned = nan(numel(offline_data.timeref),size(dataset,2));
        aligned(:,1) = offline_data.timeref;
        aligned(:,2:end) = interpolated;
    end

%% config
c = 2.998e8;

%% extract
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor;
dataMiner.setFill(fill,'STABLE');

%% extract scans

ip = dataMiner.fetchDataSetTable('AUTOMATICSCAN:IP');
if isempty(ip)
    data = [];
    return
end
idx = find(ip(:,2) == 2^config.IP);
scanTimeWindows = [ ip(idx,1)-10e3 ip(idx+1,1)+10e3 ];
fprintf('Identified %d scans in IP %d, extracting ...\n', size(scanTimeWindows,1), config.IP);

dataMiner.setFill(fill);
filledBuckets.b1 = dataMiner.fetchDataSetTable('LHC.BQM.B1:FILLED_BUCKETS');
filledBuckets.b2 = dataMiner.fetchDataSetTable('LHC.BQM.B2:FILLED_BUCKETS');

for scan=1:size(scanTimeWindows)
    fprintf('extracting scan %d ...\n', scan);
    dataMiner.setStartTimestamp(scanTimeWindows(scan,1)-10e3);
    dataMiner.setEndTimestamp(scanTimeWindows(scan,2)+10e3);

    [~,offlineMin]=min(abs(offline_data.timeref-scanTimeWindows(scan,1)));
    [~,offlineMax]=min(abs(offline_data.timeref-scanTimeWindows(scan,2)));
    offlineWindow = offlineMin:offlineMax;
    if numel(offlineWindow) < 10
        fprintf('ignoring - no offline data\n');
        continue;
    end 

    try
        data{scan}.raw.acqFlag = alignDataToOffline(dataMiner.fetchDataSetTable('AUTOMATICSCAN:ACQUISITION_FLAG'), 'previous', 0);
        data{scan}.raw.nomDisp.xing.b1 = alignDataToOffline(dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B1_XING'), 'previous', 0);
        data{scan}.raw.nomDisp.xing.b2 = alignDataToOffline(dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B2_XING'), 'previous', 0);
        data{scan}.raw.nomDisp.sep.b1 = alignDataToOffline(dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B1_SEP'), 'previous', 0);
        data{scan}.raw.nomDisp.sep.b2 = alignDataToOffline(dataMiner.fetchDataSetTable('AUTOMATICSCAN:SET_NOM_DISPLAC_B2_SEP'), 'previous', 0);
        data{scan}.raw.bunchLengths.b1 = alignDataToOffline(dataMiner.fetchDataSetTable('LHC.BQM.B1:BUNCH_LENGTHS'), 'linear', NaN);
        data{scan}.raw.bunchLengths.b2 = alignDataToOffline(dataMiner.fetchDataSetTable('LHC.BQM.B2:BUNCH_LENGTHS'), 'linear', NaN);
        data{scan}.raw.totalLumi = [offline_data.timeref offline_data.totalLumi];
        data{scan}.raw.bunchLumi = [offline_data.timeref offline_data.bunchLumi];
    catch e
        data{scan} = [];
        fprintf('Unhandled exception, skipping scan\n');
        e
    end
    

    data{scan}.meta.filledSlots.b1 = (filledBuckets.b1(end-1,find(filledBuckets.b1(end-1,2:end)>0)+1)-1)./10+1;
    data{scan}.meta.filledSlots.b2 = (filledBuckets.b2(end-1,find(filledBuckets.b2(end-1,2:end)>0)+1)-1)./10+1;
    colliding = CollidingBunchesIP(data{scan}.meta.filledSlots.b1, data{scan}.meta.filledSlots.b2, config.IP);
    data{scan}.meta.colliding.b1 = colliding(1,:);
    data{scan}.meta.colliding.b2 = colliding(2,:);
end



%% parse scans
for scan=1:size(scanTimeWindows)
    %% parse scan
    for plane='hv'
        fprintf('parsing scan %d %s\n', scan, plane);
        relPlane = config.planes.(plane);
        try
            separation = (data{scan}.raw.nomDisp.(relPlane).b1(:,2)-data{scan}.raw.nomDisp.(relPlane).b2(:,2));
            steps = unique(separation);
            steps = steps(~isnan(steps));
        catch e
            fprintf('Warning: skipping scan %d %s, no data\n', scan, plane);
            continue
        end
        
        if isempty(steps)
            fprintf('Warning: skipping scan %d %s, no data\n', scan, plane);
            continue
        end
        % windowing: only consider points in the scan
        firstPoint = find(separation == min(steps),1,'first');
        lastPoint = find(separation == max(steps),1,'last');
        window = false(size(separation));
        window(firstPoint:lastPoint) = true;
        
        bunchLength.b1 = zeros(3564,1);
        bunchLength.b1(data{scan}.meta.filledSlots.b1,1) = nanmean(data{scan}.raw.bunchLengths.b1(:,(1:numel(data{scan}.meta.filledSlots.b1))+1),1);
        bunchLength.b2 = zeros(3564,1);
        bunchLength.b2(data{scan}.meta.filledSlots.b2,1) = nanmean(data{scan}.raw.bunchLengths.b2(:,(1:numel(data{scan}.meta.filledSlots.b2))+1),1);
        data{scan}.(plane).bunchLength = zeros(1,3564);
        data{scan}.(plane).bunchLength(1,data{scan}.meta.colliding.b1) =  (bunchLength.b1(data{scan}.meta.colliding.b1) + ...
            bunchLength.b2(data{scan}.meta.colliding.b2)) .* 0.5 .* c ./ 4;

        data{scan}.(plane).crossingAngle = config.crossingAngles.(plane);

        data{scan}.(plane).bump = steps;
        for step=1:numel(steps)
            idx = (separation==steps(step)) & (data{scan}.raw.acqFlag(:,2)==1) & window;
            data{scan}.(plane).totalLumi(step,1) = mean(data{scan}.raw.totalLumi(idx,2));
            data{scan}.(plane).bunchLumi(step,:) = mean(data{scan}.raw.bunchLumi(idx,2:end),1);
        end
    end
end
end

