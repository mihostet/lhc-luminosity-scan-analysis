project for LHC emittance/lumi scan analysis

Usage Example (need to have the MATLAB data extractor from https://gitlab.cern.ch/mihostet/matlab-tools-michi set up):
```matlab
config = ConfigForIP5();
data = ExtractEmittanceScans(config, 5024);

[resultScanTwo,errorsScanTwo] = BunchEmittancesFromScan(config, data{2});
[resultScanFour,errorsScanFour] = BunchEmittancesFromScan(config, data{4});
```