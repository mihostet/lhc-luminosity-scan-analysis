function [ ret ] = nanpolyfit( array1, array2, grade )
%NANPOLYFIT Summary of this function goes here
%   Detailed explanation goes here
validdata1 = ~isnan(array1);
validdata2 = ~isnan(array2);
validdataBoth = validdata1 & validdata2;
keep1 = array1(validdataBoth) ;
keep2 = array2(validdataBoth) ;

ret = polyfit(keep1, keep2, grade);

end

