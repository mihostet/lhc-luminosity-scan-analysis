fills=[
5052
5056
5059
5060
5068
5069
5072
5073
5076
5078
5080
5083
5085
5091
5093
5095
5096
5097
5101
5102
5106
5107
5108
5109
5110
5111
5112
5116
5117
5149
5151
5154
5161
5162
5163
5169
5170
5173
5179
5181
5183
5187
5194
5196
5197
5198
5199
5205
5206
5209
5210
5211
5213
5219
5222
5223
5229
5246
5247
5251
5253
5254
5256
5257
5258
5264
5265
5266
5267
5270
5274
5275
5276
5277
5279
5282
5287];

%stats = [];
load('stats.mat')

%%
for idx=1:numel(fills);
    fill=fills(idx);
    if(~isempty(stats(idx).gas))
        fprintf('SKIPPING FILL %d\n', fill);
        continue
    end
    fprintf('================= OUTER MAIN LOOP FILL %d ==================\n', fill);
    tic
    try
        [ stats(idx).gas, stats(idx).bsrt, stats(idx).scans, stats(idx).lumis ] = HackAnalysisFill( fill );
        save('stats.mat', 'stats')
    catch e
        disp(e)
    end
    toc
end

