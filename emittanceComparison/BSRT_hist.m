clear
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
fill=5339;
dataMiner.setFill(fill, 'STABLE');

%% lsf
dataMiner.limitTimeframe(3);
dataMiner.enableScale('REPEAT',1,'HOUR');
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5R4.B1:LSF_H');
bsrtconfig.B1.h.lsf = lsf(1,2);
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5R4.B1:LSF_V');
bsrtconfig.B1.v.lsf = lsf(1,2);
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5L4.B2:LSF_H');
bsrtconfig.B2.h.lsf = lsf(1,2);
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5L4.B2:LSF_V');
bsrtconfig.B2.v.lsf = lsf(1,2);
dataMiner.disableScale();
%% config
bsrtconfig.B1.h.delay = 'LHC.BSRT.5R4.B1:GATE_DELAY';
bsrtconfig.B1.h.sigma = 'LHC.BSRT.5R4.B1:FIT_SIGMA_H';
bsrtconfig.B1.h.beta = 200;
bsrtconfig.B1.v.delay = 'LHC.BSRT.5R4.B1:GATE_DELAY';
bsrtconfig.B1.v.sigma = 'LHC.BSRT.5R4.B1:FIT_SIGMA_V';
bsrtconfig.B1.v.beta = 330;
bsrtconfig.B2.h.delay = 'LHC.BSRT.5L4.B2:GATE_DELAY';
bsrtconfig.B2.h.sigma = 'LHC.BSRT.5L4.B2:FIT_SIGMA_H';
bsrtconfig.B2.h.beta = 200;
bsrtconfig.B2.v.delay = 'LHC.BSRT.5L4.B2:GATE_DELAY';
bsrtconfig.B2.v.sigma = 'LHC.BSRT.5L4.B2:FIT_SIGMA_V';
bsrtconfig.B2.v.beta = 330;
bsrtconfig.energy = 6500;

%%
data = ExtractEmittanceScans(ConfigForIP5(), fill);
figure
for plane='hv'
dataMiner.limitTimeframe(0)

beam='B1'; 
gate_delay = dataMiner.fetchDataSetTable(bsrtconfig.(beam).(plane).delay);
fit_sigma = dataMiner.fetchDataSetTable(bsrtconfig.(beam).(plane).sigma);
emit_B1 = bsrt_bbb(fit_sigma, gate_delay, bsrtconfig.(beam).(plane).lsf, bsrtconfig.(beam).(plane).beta, bsrtconfig.energy);

beam='B2';
gate_delay = dataMiner.fetchDataSetTable(bsrtconfig.(beam).(plane).delay);
fit_sigma = dataMiner.fetchDataSetTable(bsrtconfig.(beam).(plane).sigma);
emit_B2 = bsrt_bbb(fit_sigma, gate_delay, bsrtconfig.(beam).(plane).lsf, bsrtconfig.(beam).(plane).beta, bsrtconfig.energy);

%%
start = 1000;
limit = min(size(emit_B1,1),size(emit_B2,1));

avg_emit = (emit_B1(start:limit,data{1}.meta.colliding.b1) + emit_B2(start:limit,data{1}.meta.colliding.b1)) .* 0.5;

%%
slice = 100;
nSlices = floor(size(avg_emit,1)/slice)-1;
bins = 0:0.02:5;
times = zeros(nSlices,1);
histImg = zeros(nSlices, numel(bins));

for ts=1:nSlices
    range= (((ts-1)*slice):(ts*slice))+1;
    times(ts) = (emit_B1(range(floor(slice/2)),1) - emit_B1(1,1))/3600e3;
    fprintf('time slice %d of %d\n', ts, nSlices);
    hh = hist(mean(avg_emit(range,:),1), bins);
    histImg(ts,:) = hh;
end

%%
if plane=='h'
    subplot(1,2,1);
else
    subplot(1,2,2);
end
colormap jet
imagesc(times,bins,histImg', [0 130])
set(gca,'YDir','normal')
ylim([1 4])
xlabel('Time in collisions [h]');
ylabel('Emittance [um]');

end