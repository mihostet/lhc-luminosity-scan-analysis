function [ gas, bsrt, scans, lumis ] = HackAnalysisFill( fill )
%HACKANALYSISFILL Quickly hacked over a weekend ...

dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor;
get = @(i,s) s(i);

%% beam gas

dataMiner.setFill(fill, 'STABLE');
gas_unfolded_X = dataMiner.fetchDataSetTable('LHCB:BEAM1_SIZE_UNFOLDED_X');
gas_bad_X = [(diff(gas_unfolded_X(:,2))==0); false] | (gas_unfolded_X(:,2) < 0 );
gas_fit_X = polyfit((gas_unfolded_X(~gas_bad_X,1)-gas_unfolded_X(1,1))./3600e3,1e6.*(gas_unfolded_X(~gas_bad_X,2).*1e-6).^2.*(6500/0.938)./3,1);

gas_unfolded_Y = dataMiner.fetchDataSetTable('LHCB:BEAM1_SIZE_UNFOLDED_Y');
gas_bad_Y = [(diff(gas_unfolded_Y(:,2))==0); false] | (gas_unfolded_Y(:,2) < 0 );
gas_fit_Y = polyfit((gas_unfolded_Y(~gas_bad_Y,1)-gas_unfolded_Y(1,1))./3600e3,1e6.*(gas_unfolded_Y(~gas_bad_Y,2).*1e-6).^2.*(6500/0.938)./3,1);

gas.x = gas_fit_X(2);
gas.y = gas_fit_Y(2);

fprintf('Got BEAMGAS data for fill %d -- X=%.2f Y=%.2f\n', fill, gas.x, gas.y);

%% scans
config = ConfigForIP5();
data = ExtractEmittanceScans(config, fill);

scan = 1;

%% process
[result,~] = BunchEmittancesFromScan(config, data{scan});

%%
compEmittance(1:3564) = NaN;
compOptima(1:3564) = NaN;
cLight = 299792458;

load('referenceProfile.mat');
distS.B1 = DistributionFromProfileData( profiles.B1.timeref.*cLight, profiles.B1.bunch(1,:), 'normalize');
distS.B2 = DistributionFromProfileData(-profiles.B2.timeref.*cLight, profiles.B2.bunch(1,:), 'normalize');

for bunch=data{scan}.meta.colliding.b1
    bunchIdx.B1 = find(data{scan}.meta.filledSlots.b1==bunch);
    bunchIdx.B2 = find(data{scan}.meta.filledSlots.b2==bunch);
    theLumi = data{scan}.h.bunchLumi(:,bunch);
    theBump = data{scan}.h.bump.*1e-3;
    analyticFunc = AnalyticLumi(distS.B1, distS.B2, config.crossingAngles.h, max(theLumi));
    fitresult=fit(theBump, theLumi, fittype(@(sigmax,sepx,x) analyticFunc(sigmax,sepx,x)), 'StartPoint', [20e-6 0e-6], ...
        'Lower', [1e-6 -20e-6], 'Upper', [100e-6 20e-6], 'Algorithm', 'Trust-Region', 'TolFun', 1e-15, 'TolX', 1e-15);
    compensatedSigma = coeffvalues(fitresult);
    compensatedEmittance = 1e6 * (compensatedSigma(1))^2 * (config.energy/0.938) / config.betaStar;
    compensatedOptimum = compensatedSigma(2) .* 1e6;
    compEmittance(bunch) = compensatedEmittance;
    compOptima(bunch) = compensatedOptimum;
end
%%

scans.x = nanmean(compEmittance);
scans.uncomp_x = nanmean([result.v.emittance]);
scans.y = nanmean([result.v.emittance]);
fprintf('Got SCAN data for fill %d -- X=%.2f Y=%.2f\n', fill, scans.x, scans.y);


%% lumiEmittance
dataMiner = cern.matlab.dataextractor2.MatlabLoggingDataExtractor('LDB_PRO');
dataMiner.setFill(fill,'STABLE');
dataMiner.limitTimeframe(3);

bunchIntB1 = dataMiner.fetchDataSetTable('LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
bunchIntB2 = dataMiner.fetchDataSetTable('LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY', 'LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
bunchLenB1_raw = dataMiner.fetchDataSetTable('LHC.BQM.B1:BUNCH_LENGTHS', 'LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
bunchLenB2_raw = dataMiner.fetchDataSetTable('LHC.BQM.B2:BUNCH_LENGTHS', 'LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
try
    lumi_CMS = ReadMassiFiles(fill, 'CMS', bunchIntB1);
catch e
    lumi_CMS = dataMiner.fetchDataSetTable('CMS:BUNCH_LUMI_INST', 'LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY');
    lumi_CMS(:,2:end) = 1000 .* lumi_CMS(:,2:end);
end
lumi_ATLAS = ReadMassiFiles(fill, 'ATLAS', bunchIntB1);

%% process
bunchLenB1 = zeros(size(bunchLenB1_raw));
bunchLenB2 = zeros(size(bunchLenB2_raw));
bunchLenB1(:,[1 data{1}.meta.filledSlots.b1]) = bunchLenB1_raw(:, 1:numel(data{1}.meta.filledSlots.b1)+1);
bunchLenB2(:,[1 data{1}.meta.filledSlots.b2]) = bunchLenB2_raw(:, 1:numel(data{1}.meta.filledSlots.b2)+1);

lumiEmittance_CMS = BunchEmittanceFromLumi(bunchIntB1, bunchIntB2, bunchLenB1, bunchLenB2, lumi_CMS, data{1}.meta.colliding.b1+1, 6500, 0.4, 2*185e-6);
lumiEmittance_ATLAS = BunchEmittanceFromLumi(bunchIntB1, bunchIntB2, bunchLenB1, bunchLenB2, lumi_ATLAS, data{1}.meta.colliding.b1+1, 6500, 0.4, 2*185e-6);

%% filter
lumiEmittance_CMS(lumiEmittance_CMS(:,1)>data{1}.raw.totalLumi(1,1) & lumiEmittance_CMS(:,1)<data{1}.raw.totalLumi(end,1),2:end)= NaN;
lumiEmittance_CMS(1:5,2:end)=NaN;
lumiEmittance_ATLAS(1:5,2:end)=NaN;

lumis.atlas = get(2,nanpolyfit(lumiEmittance_ATLAS(:,1)-lumiEmittance_ATLAS(1,1),nanmean(lumiEmittance_ATLAS(:,2:end),2),1)).*1e6;
lumis.cms = get(2,nanpolyfit(lumiEmittance_CMS(:,1)-lumiEmittance_CMS(1,1),nanmean(lumiEmittance_CMS(:,2:end),2),1)).*1e6;

fprintf('Got LUMI data for fill %d -- %.2f %.2f\n', fill, lumis.atlas, lumis.cms);

%% BSRT
dataMiner.setFill(fill,'STABLE');
dataMiner.limitTimeframe(3);
dataMiner.enableScale('REPEAT',1,'HOUR');
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5R4.B1:LSF_H');
bsrtconfig.B1.h.lsf = lsf(1,2);
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5R4.B1:LSF_V');
bsrtconfig.B1.v.lsf = lsf(1,2);
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5L4.B2:LSF_H');
bsrtconfig.B2.h.lsf = lsf(1,2);
lsf=dataMiner.fetchDataSetTable('LHC.BSRT.5L4.B2:LSF_V');
bsrtconfig.B2.v.lsf = lsf(1,2);
dataMiner.disableScale();
%% config
bsrtconfig.B1.h.delay = 'LHC.BSRT.5R4.B1:GATE_DELAY';
bsrtconfig.B1.h.sigma = 'LHC.BSRT.5R4.B1:FIT_SIGMA_H';
bsrtconfig.B1.h.beta = 200;

bsrtconfig.B1.v.delay = 'LHC.BSRT.5R4.B1:GATE_DELAY';
bsrtconfig.B1.v.sigma = 'LHC.BSRT.5R4.B1:FIT_SIGMA_V';
bsrtconfig.B1.v.beta = 330;

bsrtconfig.B2.h.delay = 'LHC.BSRT.5L4.B2:GATE_DELAY';
bsrtconfig.B2.h.sigma = 'LHC.BSRT.5L4.B2:FIT_SIGMA_H';
bsrtconfig.B2.h.beta = 200;

bsrtconfig.B2.v.delay = 'LHC.BSRT.5L4.B2:GATE_DELAY';
bsrtconfig.B2.v.sigma = 'LHC.BSRT.5L4.B2:FIT_SIGMA_V';
bsrtconfig.B2.v.beta = 330;
bsrtconfig.energy = 6500;


%% loop
for b = {'B1','B2'}
    beam = char(b);
    for plane = 'hv'
        %% single
%        fprintf('Loading BSRT data, %s %s\n', beam, plane);
        gate_delay = dataMiner.fetchDataSetTable(bsrtconfig.(beam).(plane).delay);
        fit_sigma = dataMiner.fetchDataSetTable(bsrtconfig.(beam).(plane).sigma);
        emit = bsrt_bbb(fit_sigma, gate_delay, bsrtconfig.(beam).(plane).lsf, bsrtconfig.(beam).(plane).beta, bsrtconfig.energy);
        filled = find(emit(2000,2:end)>0)+1;
        bsrtEmit.mean.(beam).(plane) = [ emit(:,1) (nanmean(emit(:,filled),2)) ];
        bsrtEmit.median.(beam).(plane) = [ emit(:,1) (nanmedian(emit(:,filled),2)) ];
%        emit_filtered = emit(:,filled);
%        emit_filtered(:,emit_filtered(end-5000,:)>(median(emit_filtered(end-5000,:))+0.5)) = NaN;
%        fprintf('Filtering excluded %d bunches.\n',sum(isnan(emit_filtered(1,:))));
%        bsrtEmit.filtered.(beam).(plane) = [ emit(:,1) (nanmean(emit_filtered,2)) ];
    end
end
%%
bsrt.B1H = get(2,polyfit(bsrtEmit.median.B1.h(1000:end,1)-bsrtEmit.median.B1.h(1,1),bsrtEmit.median.B1.h(1000:end,2),1));
bsrt.B1V = get(2,polyfit(bsrtEmit.median.B1.v(1000:end,1)-bsrtEmit.median.B1.v(1,1),bsrtEmit.median.B1.v(1000:end,2),1));
bsrt.B2H = get(2,polyfit(bsrtEmit.median.B2.h(1000:end,1)-bsrtEmit.median.B2.h(1,1),bsrtEmit.median.B2.h(1000:end,2),1));
bsrt.B2V = get(2,polyfit(bsrtEmit.median.B2.v(1000:end,1)-bsrtEmit.median.B2.v(1,1),bsrtEmit.median.B2.v(1000:end,2),1));

fprintf('Got BSRT data for fill %d -- X=%.2f Y=%.2f\n', fill, (bsrt.B1H+bsrt.B2H)/2, (bsrt.B1V+bsrt.B2V)/2);

%% export
clear dataMiner
save(sprintf('hackcache_fill_%d.mat',fill));
end

